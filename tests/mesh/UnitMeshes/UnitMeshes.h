#include <dolfin_tests.h>

#ifdef HAVE_CHECK

#include <dolfin/mesh/Box.h>
#include <dolfin/mesh/unitmeshes/UnitCube.h>
#include <dolfin/mesh/unitmeshes/UnitDisk.h>
#include <dolfin/mesh/unitmeshes/UnitInterval.h>
#include <dolfin/mesh/unitmeshes/UnitSquare.h>

using namespace dolfin;

//-----------------------------------------------------------------------------
int test_mesh( Mesh & mesh )
{
  mesh.refine();
  mesh.refine();
  return 0;
}
//-----------------------------------------------------------------------------
DOLFIN_START_TEST( test_UnitInterval )
{
  dolfin::uint const Nx = 2;

  // Interval
  UnitInterval mesh( Nx );
  ck_assert( test_mesh( mesh ) == 0 );
}
DOLFIN_END_TEST
//-----------------------------------------------------------------------------
DOLFIN_START_TEST( test_UnitSquare )
{
  dolfin::uint const Nx = 2;
  dolfin::uint const Ny = 4;

  // Square
  std::vector< UnitSquare::Type > types;
  types.push_back( UnitSquare::right );
  types.push_back( UnitSquare::left );
  types.push_back( UnitSquare::crisscross );
  for ( dolfin::uint tp = 0; tp < types.size(); ++tp )
  {
    UnitSquare mesh( Nx, Ny, types[tp] );
    ck_assert( test_mesh( mesh ) == 0 );
  }
}
DOLFIN_END_TEST
//-----------------------------------------------------------------------------
DOLFIN_START_TEST( test_UnitCube )
{
  dolfin::uint const Nx = 2;
  dolfin::uint const Ny = 4;
  dolfin::uint const Nz = 8;

  // Cube
  UnitCube mesh( Nx, Ny, Nz );
  ck_assert( test_mesh( mesh ) == 0 );
}
DOLFIN_END_TEST
//-----------------------------------------------------------------------------
DOLFIN_START_TEST( test_Box )
{
  // Box
  dolfin::uint const Nx = 2;
  dolfin::uint const Ny = 4;
  dolfin::uint const Nz = 8;

  real a = 0.0;
  real b = 4.0;
  real c = 0.0;
  real d = 2.0;
  real e = 0.0;
  real f = 1.0;
  Box  mesh( a, b, c, d, e, f, Nx, Ny, Nz );
  ck_assert( test_mesh( mesh ) == 0 );
}
DOLFIN_END_TEST
//-----------------------------------------------------------------------------
DOLFIN_START_TEST( test_UnitDisk )
{
  // Disk
  dolfin::uint const      Nx = 2;
  std::vector< UnitDisk::Type > types;
  types.push_back( UnitDisk::right );
  types.push_back( UnitDisk::left );
  types.push_back( UnitDisk::crisscross );
  std::vector< UnitDisk::Transformation > trans;
  trans.push_back( UnitDisk::maxn );
  trans.push_back( UnitDisk::sumn );
  trans.push_back( UnitDisk::rotsumn );
  for ( dolfin::uint tp = 0; tp < types.size(); ++tp )
  {
    for ( dolfin::uint tr = 0; tr < trans.size(); ++tr )
    {
      UnitDisk mesh( Nx, types[tp], trans[tr] );
      ck_assert( test_mesh( mesh ) == 0 );
    }
  }
}
DOLFIN_END_TEST
//-----------------------------------------------------------------------------

#endif
