#include <dolfin_tests.h>

#ifdef HAVE_CHECK

#include <dolfin/fem/Functional.h>
#include <dolfin/fem/LinearForm.h>
#include <dolfin/fem/BilinearForm.h>
#include <dolfin/fem/CoefficientMap.h>
#include <dolfin/mesh/unitmeshes/UnitSquare.h>

using namespace dolfin;

//-----------------------------------------------------------------------------
DOLFIN_START_TEST( test_Functional )
  {
  }
DOLFIN_END_TEST
//-----------------------------------------------------------------------------
DOLFIN_START_TEST( test_LinearForm )
  {
  }
DOLFIN_END_TEST
//-----------------------------------------------------------------------------
DOLFIN_START_TEST( test_BilinearForm )
  {
  }
DOLFIN_END_TEST
//-----------------------------------------------------------------------------

#endif
