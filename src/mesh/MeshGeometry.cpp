// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.

#include <dolfin/mesh/MeshGeometry.h>

#include <dolfin/common/constants.h>
#include <dolfin/common/types.h>
#include <dolfin/log/log.h>
#include <dolfin/math/basic.h>

#include <algorithm>
#include <ctime>
#include <string>

namespace dolfin
{

//-----------------------------------------------------------------------------

MeshGeometry::MeshGeometry( Space const & space, size_t size )
  : space_( space.clone() )
  , dim_( space.dim() )
  , size_( 0 )
  , coordinates_( std::vector< real >( size_, 0.0 ) )
  , abs_tol_( std::vector< real >( dim_ + 1, 0.0 ) )
  , timestamp_( 0 )
{
  resize( size );
}

//-----------------------------------------------------------------------------

MeshGeometry::MeshGeometry( MeshGeometry const & other )
  : space_( other.space_->clone() )
  , dim_( other.dim_ )
  , size_( other.size_ )
  , coordinates_( other.coordinates_ )
  , abs_tol_( other.abs_tol_ )
  , timestamp_( other.timestamp_ )
{
}

//-----------------------------------------------------------------------------

MeshGeometry::~MeshGeometry()
{
  if ( space_ )
    delete space_;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator=( MeshGeometry const & other ) -> MeshGeometry &
{
  MeshGeometry tmp( other );
  swap( *this, tmp );

  return *this;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator==( MeshGeometry const & other ) const -> bool
{
  if ( *space_ != *other.space_ )
  {
    return false;
  }
  if ( size_ != other.size_ )
  {
    return false;
  }
  if ( dim_ != other.dim_ )
  {
    return false;
  }
  if ( coordinates_ != other.coordinates_ )
  {
    return false;
  }
  if ( abs_tol_ != other.abs_tol_ )
  {
    return false;
  }

  return true;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator!=( MeshGeometry const & other ) const -> bool
{
  return !( *this == other );
}

//-----------------------------------------------------------------------------

void swap( MeshGeometry & a, MeshGeometry & b )
{
  using std::swap;

  swap( a.space_, b.space_ );
  swap( a.dim_, b.dim_ );
  swap( a.size_, b.size_ );
  swap( a.coordinates_, b.coordinates_ );
  swap( a.abs_tol_, b.abs_tol_ );
  swap( a.timestamp_, b.timestamp_ );
}

//-----------------------------------------------------------------------------

void MeshGeometry::resize( size_t size )
{
  if ( size != size_ )
  {
    if ( size )
    {
      // PointCells have dim == 0, so special treatment is necessary
      coordinates_.resize( ( dim_ > 0 ) ? dim_ * size : size );
      size_ = size;
    }
    else
    {
      coordinates_.clear();
      size_ = 0;
    }
  }
  update_token();
}

//-----------------------------------------------------------------------------

void MeshGeometry::finalize()
{
  if ( size_ > 0 && coordinates_.empty() )
  {
    error( "MeshGeometry : empty coordinates for non-empty geometry" );
  }
  // Invalidate dependencies
  update_token();
}

//-----------------------------------------------------------------------------

void MeshGeometry::set_abs_tolerance( size_t dim, real atol )
{
  dolfin_assert( dim <= dim_ );
  if ( atol <= 0.0 )
  {
    warning( "MeshGeometry : tolerance '%g' for dimension %u is non-positive",
             atol,
             dim );
  }
  abs_tol_[dim] = std::fabs( atol );
}

//-----------------------------------------------------------------------------

void MeshGeometry::assign( std::vector< real > const & coordinates )
{
  if ( coordinates.size() % dim_ )
  {
    error( "MeshGeometry : size mismatch in coordinates assignment" );
  }
  coordinates_.resize( coordinates.size() );
  std::copy( coordinates.begin(), coordinates.end(), coordinates_.begin() );
  size_ = coordinates.size() / dim_;
}

//-----------------------------------------------------------------------------

void MeshGeometry::remap( std::vector< size_t > const & mapping )
{
  if ( mapping.size() != size_ )
  {
    error( "MeshGeometry : size mismatch for remapping of coordinates " );
  }

  // Reorder coordinates w.r.t old -> new index mapping
  std::vector< real > xcpy( dim_ * size_, 0.0 );
  for ( size_t i = 0; i < size_; ++i )
  {
    real const * x = coordinates_.data() + i * dim_;
    std::copy( x, x + dim_, xcpy.data() + mapping[i] * dim_ );
  }
  coordinates_ = xcpy;

  // Invalidate dependencies
  update_token();
}

//-----------------------------------------------------------------------------

void MeshGeometry::assign( MeshGeometry const &          other,
                           std::vector< size_t > const & mapping )
{
  if ( this == &other )
  {
    error( "MeshGeometry : assignment of coordinates to self" );
  }
  if ( other.dim() != dim_ )
  {
    error( "MeshGeometry : dimension mismatch for assignment of coordinates " );
  }
  if ( mapping.size() != size_ )
  {
    error( "MeshGeometry : size mismatch for assignment of coordinates " );
  }
  for ( size_t i = 0; i < mapping.size(); ++i )
  {
    std::copy( other.x( mapping[i] ),
               other.x( mapping[i] ) + dim_,
               coordinates_.data() + i * dim_ );
  }
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator*=( real const a ) -> MeshGeometry &
{
  typedef std::vector< real >::iterator CoordIter;
  for ( CoordIter it = coordinates_.begin(); it != coordinates_.end(); ++it )
  {
    *it *= a;
  }

  return *this;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator/=( real const a ) -> MeshGeometry &
{
  if ( small( a ) )
  {
    error( "MeshGeometry : dividing coordinates by zero" );
  }
  real const b = 1.0 / a;

  using CoordIter = std::vector< real >::iterator;

  for ( CoordIter it = coordinates_.begin(); it != coordinates_.end(); ++it )
  {
    *it *= b;
  }
  return *this;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator+=( real const a ) -> MeshGeometry &
{
  using CoordIter = std::vector< real >::iterator;

  for ( CoordIter it = coordinates_.begin(); it != coordinates_.end(); ++it )
  {
    *it += a;
  }
  return *this;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator-=( real const a ) -> MeshGeometry &
{
  using CoordIter = std::vector< real >::iterator;

  for ( CoordIter it = coordinates_.begin(); it != coordinates_.end(); ++it )
  {
    *it -= a;
  }

  return *this;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator+=( Point const & p ) -> MeshGeometry &
{
  using CoordIter = std::vector< real >::iterator;

  for ( CoordIter it = coordinates_.begin(); it != coordinates_.end(); )
  {
    for ( size_t i = 0; i < dim_; ++i )
    {
      *it += p[i];
      ++it;
    }
  }
  return *this;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator-=( Point const & p ) -> MeshGeometry &
{
  using CoordIter = std::vector< real >::iterator;

  for ( CoordIter it = coordinates_.begin(); it != coordinates_.end(); )
  {
    for ( size_t i = 0; i < dim_; ++i )
    {
      *it += p[i];
      ++it;
    }
  }

  return *this;
}

//-----------------------------------------------------------------------------

auto MeshGeometry::token() const -> int
{
  return timestamp_ ^ size_;
}

//-----------------------------------------------------------------------------

void MeshGeometry::update_token()
{
  timestamp_ = std::time( nullptr );
}

//-----------------------------------------------------------------------------

void MeshGeometry::disp() const
{
  section( "MeshGeometry" );
  prm( "dimension", dim_ );
  prm( "size", size_ );
  end();
}

//-----------------------------------------------------------------------------

void MeshGeometry::dump() const
{
  for ( size_t i = 0; i < size_; ++i )
  {
    cout << i << ":";
    for ( size_t d = 0; d < dim_; ++d )
    {
      cout << " " << x( i )[d];
    }
    cout << "\n";
  }
}

//-----------------------------------------------------------------------------

auto MeshGeometry::operator>>( std::vector< real > & ) const
  -> MeshGeometry const &
{
  error( "MeshGeometry::operator>> unimplemented / deprecated." );
  // A.assign(coordinates_.data(), coordinates_.data() + dim_ * size_);
  // A %= dim_;
  return *this;
}

//-----------------------------------------------------------------------------

} /* namespace dolfin */
