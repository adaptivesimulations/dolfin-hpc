// Copyright (C) 2005-2006 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.

#include <dolfin/mesh/Rectangle.h>

#include <dolfin/main/MPI.h>
#include <dolfin/mesh/EuclideanSpace.h>
#include <dolfin/mesh/MeshEditor.h>

namespace dolfin
{

//-----------------------------------------------------------------------------
Rectangle::Rectangle( real   a,
                      real   b,
                      real   c,
                      real   d,
                      size_t nx,
                      size_t ny,
                      Type   type )
  : Mesh( *CellType::create( CellType::triangle ), EuclideanSpace( 2 ) )
{
  if ( nx < 1 || ny < 1 )
    error( "Size of unit square must be at least 1 in each dimension." );

  rename( "mesh", "Mesh of the unit square (a,b) x (c,d)" );
  // Open mesh for editing
  MeshEditor editor( *this, this->type(), this->space() );

  // Create vertices and cells:
  if ( type == crisscross )
  {
    editor.init_vertices( ( nx + 1 ) * ( ny + 1 ) + nx * ny );
    editor.init_cells( 4 * nx * ny );
  }
  else
  {
    editor.init_vertices( ( nx + 1 ) * ( ny + 1 ) );
    editor.init_cells( 2 * nx * ny );
  }

  // Create main vertices:
  size_t vertex = 0;
  real   x[2]   = { 0.0 };
  for ( size_t iy = 0; iy <= ny; iy++ )
  {
    x[1] = c
           + ( ( static_cast< real >( iy ) ) * ( d - c )
               / static_cast< real >( ny ) );
    for ( size_t ix = 0; ix <= nx; ix++ )
    {
      x[0] = a
             + ( ( static_cast< real >( ix ) ) * ( b - a )
                 / static_cast< real >( nx ) );
      editor.add_vertex( vertex++, x );
    }
  }

  // Create midpoint vertices if the mesh type is crisscross
  if ( type == crisscross )
  {
    for ( size_t iy = 0; iy < ny; iy++ )
    {
      x[1] = c
             + ( static_cast< real >( iy ) + 0.5 ) * ( d - c )
                 / static_cast< real >( ny );
      for ( size_t ix = 0; ix < nx; ix++ )
      {
        x[0] = a
               + ( static_cast< real >( ix ) + 0.5 ) * ( b - a )
                   / static_cast< real >( nx );
        editor.add_vertex( vertex++, x );
      }
    }
  }

  // Create triangles
  size_t cell = 0;
  if ( type == crisscross )
  {
    for ( size_t iy = 0; iy < ny; iy++ )
    {
      for ( size_t ix = 0; ix < nx; ix++ )
      {
        size_t const v0   = iy * ( nx + 1 ) + ix;
        size_t const v1   = v0 + 1;
        size_t const v2   = v0 + ( nx + 1 );
        size_t const v3   = v1 + ( nx + 1 );
        size_t const vmid = ( nx + 1 ) * ( ny + 1 ) + iy * nx + ix;

        // Note that v0 < v1 < v2 < v3 < vmid.
        size_t const connectivity[12] = {
          v0, v1, vmid, v0, v2, vmid, v1, v3, vmid, v2, v3, vmid };

        editor.add_cell( cell++, &connectivity[0] );
        editor.add_cell( cell++, &connectivity[3] );
        editor.add_cell( cell++, &connectivity[6] );
        editor.add_cell( cell++, &connectivity[9] );
      }
    }
  }
  else if ( type == left )
  {
    for ( size_t iy = 0; iy < ny; iy++ )
    {
      for ( size_t ix = 0; ix < nx; ix++ )
      {
        size_t const v0 = iy * ( nx + 1 ) + ix;
        size_t const v1 = v0 + 1;
        size_t const v2 = v0 + ( nx + 1 );
        size_t const v3 = v1 + ( nx + 1 );

        size_t const connectivity[12] = { v0, v1, v2, v1, v2, v3 };

        editor.add_cell( cell++, &connectivity[0] );
        editor.add_cell( cell++, &connectivity[3] );
      }
    }
  }
  else
  {
    for ( size_t iy = 0; iy < ny; iy++ )
    {
      for ( size_t ix = 0; ix < nx; ix++ )
      {
        size_t const v0 = iy * ( nx + 1 ) + ix;
        size_t const v1 = v0 + 1;
        size_t const v2 = v0 + ( nx + 1 );
        size_t const v3 = v1 + ( nx + 1 );

        size_t const connectivity[12] = { v0, v1, v3, v0, v2, v3 };

        editor.add_cell( cell++, &connectivity[0] );
        editor.add_cell( cell++, &connectivity[3] );
      }
    }
  }

  // Close mesh editor
  editor.close();
}
//-----------------------------------------------------------------------------

}
