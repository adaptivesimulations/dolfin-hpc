// Copyright (C) 2014 Aurelien Larcher.
// Licensed under the GNU GPL Version 2.

#include <dolfin/evolution/Time.h>

#include <dolfin/log/log.h>
#include <dolfin/math/basic.h>

namespace dolfin
{

//-----------------------------------------------------------------------------
Time::Time( real T_start, real T_end, real T_current )
  : T_( T_start, T_end )
  , sign_( ( T_start < T_end ) - ( T_end < T_start ) )
  , t_( T_current )
{
}
//-----------------------------------------------------------------------------
Time::Time( Interval I )
  : T_( I )
  , sign_( ( I.first < I.second ) - ( I.second < I.first ) )
  , t_( I.first )
{
}
//-----------------------------------------------------------------------------
auto Time::interval() const -> std::pair<real, real> const&
{
  return T_;
}
//-----------------------------------------------------------------------------
auto Time::sign() const -> int
{
  return sign_;
}
//-----------------------------------------------------------------------------
auto Time::is_valid(real atol) const -> bool
{
  return std::fabs(t_ - T_.first)  <= std::fabs(T_.second - T_.first) + atol
      && std::fabs(t_ - T_.second) <= std::fabs(T_.second - T_.first) + atol;
}
//-----------------------------------------------------------------------------
auto Time::begin() const -> real
{
  return T_.first;
}
//-----------------------------------------------------------------------------
auto Time::end() const -> real
{
  return T_.second;
}
//-----------------------------------------------------------------------------
auto Time::measure() const -> real
{
  return std::fabs(T_.second - T_.first);
}
//-----------------------------------------------------------------------------
auto Time::clock() const -> real const&
{
  return t_;
}
//-----------------------------------------------------------------------------
auto Time::clock() -> real&
{
  return t_;
}
//-----------------------------------------------------------------------------
auto Time::elapsed() const -> real
{
  return std::fabs(t_ - T_.first);
}
//-----------------------------------------------------------------------------
auto Time::remaining() const -> real
{
  return std::fabs(T_.second - t_);
}
//-----------------------------------------------------------------------------
auto Time::elapsed_normalized() const -> real
{
  if(abscmp(T_.second, T_.first))
  {
    return 0.0;
  }
  return std::fabs(t_ - T_.first)/std::fabs(T_.second - T_.first);
}
//-----------------------------------------------------------------------------
auto Time::remaining_normalized() const -> real
{
  if(abscmp(T_.second, T_.first))
  {
    return 0.0;
  }
  return std::fabs(T_.second - t_)/std::fabs(T_.second - T_.first);
}
//-----------------------------------------------------------------------------
void Time::show( std::string const info ) const
{
  real const p = 100.0 * this->elapsed() / (sign_ == 0 ? 1.0 : this->measure());

  std::string const nfo = info.substr( 0, std::min( 36ul, info.size() ) );
  std::string const len = std::to_string( 48 - nfo.size() );
  std::string const msg = "t = %-" + len + ".6e %s [ %6.2f ]";

  message("----------------------------------------------------------------");
  message( msg.c_str(), t_, nfo.c_str(), p );
  message("----------------------------------------------------------------");
}
//-----------------------------------------------------------------------------
void Time::disp() const
{
  dolfin::section("Time");
  message("T0        : %f", T_.first);
  message("T1        : %f", T_.second);
  message("t         : %f", t_);
  message("valid     : %d", this->is_valid());
  message("sign      : %d", this->sign());
  message("measure   : %f", this->measure());
  message("elapsed   : %f", this->elapsed());
  message("remaining : %f", this->remaining());
  dolfin::end();
}
//-----------------------------------------------------------------------------

} /* namespace dolfin */
