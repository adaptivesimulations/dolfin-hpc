// Copyright (C) 2010 Niclas Jansson.
// Licensed under the GNU LGPL Version 2.1.

#include <dolfin/config/dolfin_config.h>

#ifdef HAVE_JANPACK

#include <dolfin/la/janpackJANPACKKrylovSolver.h>

#include <dolfin/la/janpackJANPACKMat.h>
#include <dolfin/la/janpackJANPACKVec.h>

#ifdef DOLFIN_HAVE_MPI
#include <dolfin/main/MPI.h>
#endif

#include <janpack/ilu.h>
#include <janpack/krylov_solver.h>

using namespace dolfin;

//-----------------------------------------------------------------------------

JANPACKKrylovSolver::JANPACKKrylovSolver( SolverType         method,
                                          PreconditionerType pc )
  : method( method )
  , pc_janpack( pc )
  , ksp_init( false )
#ifdef HAVE_JANPACK_MPI
  , ksp( &_ksp )
#endif
{
}

//-----------------------------------------------------------------------------

JANPACKKrylovSolver::~JANPACKKrylovSolver()
{
  jp_ksp_free( ksp );
}

//-----------------------------------------------------------------------------

dolfin::size_t JANPACKKrylovSolver::solve( const JANPACKMat & A,
                                           JANPACKVec &       x,
                                           const JANPACKVec & b )
{
  // Check dimensions
  size_t M = A.size( 0 );
  size_t N = A.size( 1 );
  if ( N != b.size() )
    error( "Non-matching dimensions for linear system." );

  // Write a message
  message( "Solving linear system of size %d x %d (Krylov solver).", M, N );

  // Reinitialize solution vector if necessary
  if ( x.local_size() != b.local_size() )
    x.init( b.local_size() );

  // Initialize preconditioner
  if ( ksp_init == false )
  {
    jp_ksp_init( ksp );
    ksp_init = true;
  }

  if ( get( "Krylov keep PC" ) )
    jp_ksp_freeze_pc( ksp );

  jp_pc_t pc_type;
  if ( pc_janpack == none )
    pc_type = JP_PC_NONE;
  else if ( pc_janpack == jacobi )
    pc_type = JP_PC_JACOBI;
  else if ( pc_janpack == ilu )
    pc_type = JP_PC_ILU;
  else if ( pc_janpack == dilu )
    pc_type = JP_PC_DILU;
  else if ( pc_janpack == amg )
  {
#ifndef HAVE_JANPACK_MPI
    pc_type = JP_PC_AMG;
#else
    warning( "The algebraic multigrid preconditioner is "
             "only available in the UPC version of JANPACK" );
    pc_type = JP_PC_NONE;
#endif
  }
  else
    pc_type = JP_PC_NONE;

  int num_iterations;
  num_iterations = jp_krylov_solver( A.mat(),
                                     x.vec(),
                                     b.vec(),
                                     ksp,
                                     ( jp_solver_t ) getType( method ),
                                     pc_type,
                                     get( "Krylov maximum iterations" ),
                                     get( "Krylov relative tolerance" ) );

  message( "Krylov solver converged in %d iterations.", num_iterations );
  return num_iterations;
}

//-----------------------------------------------------------------------------

int JANPACKKrylovSolver::getType( SolverType method ) const
{

  switch ( method )
  {
    case bicgstab:
      return JP_BICGSTAB;
    case pipebicgstab:
      return JP_PIPEBICGSTAB;
    case cg:
      return JP_CG;
    case pipecg:
      return JP_PIPECG;
    case asyncg:
      return JP_ASYNCG;
    default:
      warning( "Requested Krylov method unknown. Using BICGSTAB." );
      return JP_BICGSTAB;
  }
}

//-----------------------------------------------------------------------------

#endif
