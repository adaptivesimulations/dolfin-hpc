// Copyright (C) 2021 Julian Hornich
// Licensed under the GNU LGPL Version 2.1.

#include <dolfin/config/dolfin_config.h>

#ifdef HAVE_TRILINOS

#include <dolfin/la/trilinos/TrilinosMatrix.h>

#include <dolfin/la/trilinos/TrilinosFactory.h>

#include <numeric>

namespace dolfin
{

namespace trilinos
{

//-----------------------------------------------------------------------------

Matrix::Matrix()
  : Variable( "A", "a sparse Matrix" )
	, mat_( nullptr )
{
  // do nothing
}

//-----------------------------------------------------------------------------

Matrix::Matrix( size_t M, size_t N, bool distributed )
  : Variable( "A", "a sparse Matrix" )
	, mat_( nullptr )
{
	init( M, N, distributed );
}

//-----------------------------------------------------------------------------

Matrix::Matrix( Matrix const & A )
  : Variable( "A", "a sparse Matrix" )
	, mat_( nullptr )
{
	// delegate to assignment operator
  *this = A;
}

//-----------------------------------------------------------------------------

Matrix::~Matrix()
{
}

//-----------------------------------------------------------------------------

auto Matrix::copy() const -> Matrix *
{
  return new trilinos::Matrix( *this );
}

//-----------------------------------------------------------------------------

auto Matrix::size( size_t dim ) const -> size_t
{
  size_t size = DOLFIN_SIZE_T_MAX;

  if ( dim == 0 )
  {
    size = mat_->getColMap()->getMaxAllGlobalIndex() + 1;
  }
  else if ( dim == 1 )
  {
    size = mat_->getRowMap()->getMaxAllGlobalIndex() + 1;
  }

  return size;
}

//-----------------------------------------------------------------------------

auto Matrix::zero() -> void
{
  dolfin_assert( not mat_.is_null() );

  if ( mat_->isFillComplete() )
    mat_->resumeFill();

  mat_->setAllToScalar( 0.0 );
}

//-----------------------------------------------------------------------------

auto Matrix::apply( FinalizeType finaltype ) -> void
{
  dolfin_assert( not mat_.is_null() );

  if ( finaltype == FINALIZE or finaltype == FLUSH )
  {
    if ( mat_->isFillActive() and not mat_->isFillComplete() )
    {
      mat_->fillComplete();
    }
  }
  else
  {
    error( "trilinos::Matrix: Unknown apply mode" );
  }
}

//-----------------------------------------------------------------------------

auto Matrix::disp( size_t ) const -> void
{
  if ( mat_->isFillComplete() )
  {
    std::stringstream ss;
    mat_->print( ss );
    message( ss.str() );
  }
}

//-----------------------------------------------------------------------------

auto Matrix::init( const GenericSparsityPattern & sparsity_pattern ) -> void
{
  SparsityPattern const & spattern = reinterpret_cast< SparsityPattern const & >( sparsity_pattern );

  GO const     letTPetraDecide = Teuchos::OrdinalTraits< GO >::invalid();
  size_t const pe_rank         = MPI::rank();
  size_t const nLocalRows      = spattern.size( 0 );

  // calculate the global row indices [ lRowRange[0]; lRowRange[1] [
  std::vector< GO > gRowIndices( nLocalRows );
  std::vector< size_t > lRowRange( 2, DOLFIN_SIZE_T_MAX );
  spattern.get_range( pe_rank, lRowRange.data() );
  dolfin_assert( nLocalRows == lRowRange[1] - lRowRange[0] );
  std::iota( gRowIndices.begin(), gRowIndices.end(), lRowRange[0] );

  // create the range
  Teuchos::RCP< TPMap const > range_map( new TPMap( letTPetraDecide,
                                                    gRowIndices.data(),
                                                    gRowIndices.size(),
                                                    0, comm_ ) );

  // Get number of non-zeros per row to allocate storage
  std::vector< size_t > nzrow( nLocalRows );
  sparsity_pattern.numNonZeroPerRow( nzrow.data() );
  Teuchos::ArrayView< GO > nnz( nzrow.data(), nzrow.size() );

  // Create a non-overlapping row map for the graph
  Teuchos::RCP< TPGraph > crs_graph( new TPGraph( range_map, nnz, Tpetra::StaticProfile ) );

  _ordered_set< GO > gColset;

  for ( size_t i = 0; i < nLocalRows; ++i )
  {
    std::vector< size_t > entries;

    // diagonal portion, entries is automatically resized
    spattern.diagonal_entries( i, entries );
    std::vector< GO > indices( entries.begin(), entries.end() );
    gColset.insert( entries.begin(), entries.end() );

    // off-diagonal portion, entries is automatically resized
    spattern.off_diagonal_entries( i, entries );
    indices.insert( indices.end(), entries.begin(), entries.end() );

    dolfin_assert( nzrow[i] == indices.size() );

    Teuchos::ArrayView< GO > indices_view( indices );
    crs_graph->insertGlobalIndices( gRowIndices[i], indices_view );
  }

  dolfin_assert( spattern.size( 1 ) == gColset.size() );
  std::vector< GO > gColIndices( gColset.begin(), gColset.end() );
  Teuchos::RCP< TPMap const > domain_map( new TPMap( letTPetraDecide,
                                                     gColIndices.data(),
                                                     gColIndices.size(),
                                                     0, comm_ ) );

  // finalize the graph
  crs_graph->fillComplete( domain_map, range_map );

  // finally create the matrix from the CRS graph
  mat_ = Teuchos::rcp( new TPMatrix( crs_graph ) );
}

//-----------------------------------------------------------------------------

auto Matrix::init( size_t M, size_t N ) -> void
{
  init( M, N, true );
}

//-----------------------------------------------------------------------------

auto Matrix::init( size_t M, size_t N, bool ) -> void
{
  size_t const pe_rank = MPI::rank();
  size_t const pe_size = MPI::size();

  size_t const nLocalRows = M;
  size_t const nLocalCols = N;

  std::vector< size_t > globalRows( pe_size, 0 );
  std::vector< size_t > globalCols( pe_size, 0 );

  MPI::all_gather( nLocalRows, globalRows );
  MPI::all_gather( nLocalCols, globalCols );

  size_t const nGlobalRows = std::accumulate( globalRows.begin(), globalRows.end(), 0);
  size_t const nGlobalCols = std::accumulate( globalCols.begin(), globalCols.end(), 0);

  // Create the row and column index lists on each processor
  std::vector< GO > rowIndices( nLocalRows, 0 );
  std::vector< GO > colIndices( nLocalCols, 0 );
  {
    GO rowStart = std::accumulate( globalRows.begin(), globalRows.begin() + pe_rank, 0 );
    std::iota(rowIndices.begin(), rowIndices.end(), rowStart);

    GO colStart = std::accumulate( globalCols.begin(), globalCols.begin() + pe_rank, 0 );
    std::iota(colIndices.begin(), colIndices.end(), colStart);
  }

  using Teuchos::rcp;
  using Teuchos::RCP;

  // Create the row map
  RCP< TPMap const > rowMap = rcp( new TPMap( nGlobalRows, rowIndices.data(),
                                              nLocalRows, indexBase, comm_ ) );

  // Create the column map
  RCP< TPMap const > colMap = rcp( new TPMap( nGlobalCols, colIndices.data(),
                                              nLocalCols, indexBase, comm_ ) );

  // Create a Tpetra sparse matrix whose rows have distribution
  // given by the row Map and column Map.
  mat_ = RCP< TPMatrix >( new TPMatrix( rowMap, colMap, 120 ) );
}

//-----------------------------------------------------------------------------

auto Matrix::get( real *, size_t, const size_t *, size_t, const size_t * ) const
  -> void
{
  error( "trilinos::Matrix: not yet implemented." );
}

//-----------------------------------------------------------------------------

auto Matrix::set( const real *   block,
                  size_t         m,
                  const size_t * rows,
                  size_t         n,
                  const size_t * cols ) -> void
{
  dolfin_assert( not mat_.is_null() );

  if ( mat_->isFillComplete() )
    mat_->resumeFill();

  for ( size_t i = 0; i < m; ++i )
    mat_->replaceGlobalValues( rows[i], n, block + i * n, cols );
}

//-----------------------------------------------------------------------------

auto Matrix::add( const real *   block,
                  size_t         m,
                  const size_t * rows,
                  size_t         n,
                  const size_t * cols ) -> void
{
  dolfin_assert( not mat_.is_null() );

  if ( mat_->isFillComplete() )
    mat_->resumeFill();

  for ( size_t i = 0; i < m; ++i )
    mat_->sumIntoGlobalValues( rows[i], n, block + i * n, cols );
}
//-----------------------------------------------------------------------------

auto Matrix::norm( std::string norm_type ) const -> real
{
  real norm = 0.0;

  if ( norm_type == "l1" )
  {
    error( "trilinos::Matrix: unimplemented L1 norm." );
  }
  else if ( norm_type == "linf" )
  {
    error( "trilinos::Matrix: unimplemented inf norm." );
  }
  else if ( norm_type == "frobenius" )
  {
    norm = mat_->getFrobeniusNorm();
  }
  else
  {
    error( "trilinos::Matrix: Unknown norm type." );
  }

  return norm;
}

//-----------------------------------------------------------------------------

auto Matrix::getrow( size_t                  row,
                     std::vector< size_t > & columns,
                     std::vector< real > &   values ) const -> void
{
  dolfin_assert( not mat_.is_null() );

  size_t const ncols = mat_->getNumEntriesInGlobalRow( row );

  if ( ncols != 0 and ncols != Teuchos::OrdinalTraits< GO >::invalid() )
  {
    columns.resize( ncols );
    values.resize( ncols );

    Teuchos::ArrayView< GO >   _columns( columns );
    Teuchos::ArrayView< real > _values( values );

    size_t n = 0;
    mat_->getGlobalRowCopy( row, _columns, _values, n );

    dolfin_assert( n == ncols );
  }
}

//-----------------------------------------------------------------------------

auto Matrix::setrow( size_t                        row,
                     const std::vector< size_t > & columns,
                     const std::vector< real > &   values ) -> void
{
  dolfin_assert( not mat_.is_null() );

  if ( mat_->isFillComplete() )
    mat_->resumeFill();

  if ( columns.size() != values.size() )
  {
    error( "trilinos::Matrix: Number of columns and values don't match" );
  }

  // Handle case n = 0
  if ( columns.size() != 0 )
  {
    // Tpetra View of column indices
    Teuchos::ArrayView< GO const> column_idx( columns );

    // Tpetra View of values
    Teuchos::ArrayView< real const > data( values );

    mat_->replaceGlobalValues( row, column_idx, data );
  }
}

//-----------------------------------------------------------------------------

auto Matrix::zero( size_t m, const size_t * rows ) -> void
{
  dolfin_assert( not mat_.is_null() );

  if ( mat_->isFillComplete() )
    mat_->resumeFill();

  for ( size_t i = 0; i < m; ++i )
  {
    size_t const ncols = mat_->getNumEntriesInGlobalRow( rows[i] );

    if ( ncols != Teuchos::OrdinalTraits< GO >::invalid() )
    {
      std::vector< GO >          cols( ncols );
      std::vector< real >        data( ncols );
      Teuchos::ArrayView< GO >   cols_view( cols );
      Teuchos::ArrayView< real > data_view( data );

      // fetch the column indices
      size_t n = 0;
      mat_->getGlobalRowCopy( rows[i], cols_view, data_view, n );
      dolfin_assert( n == ncols );

      // set all columns in this row to zero
      std::fill( data.data(), data.data() + n, 0.0 );
      mat_->replaceGlobalValues( rows[i], cols_view, data_view );
    }
  }
}
//-----------------------------------------------------------------------------

auto Matrix::ident( size_t m, const size_t * rows ) -> void
{
  dolfin_assert( not mat_.is_null() );

  if ( mat_->isFillComplete() )
    mat_->resumeFill();

  // Clear affected rows to zero
  zero( m, rows );

  real const one = 1;

  // Set diagonal entries where possible
  for ( size_t i = 0; i < m; ++i )
    mat_->replaceGlobalValues( rows[i], 1, &one, &rows[i] );
}

//-----------------------------------------------------------------------------

auto Matrix::mult( const GenericVector & x,
                   GenericVector &       y,
                   bool                  transposed ) const -> void
{
  dolfin_assert( not mat_.is_null() );

  trilinos::Vector const & X = x.down_cast< trilinos::Vector const >();
  trilinos::Vector &       Y = y.down_cast< trilinos::Vector >();

  dolfin_assert( not X.vec().is_null() );
  dolfin_assert( not Y.vec().is_null() );

  if ( not transposed )
  {
    if ( size( 0 ) != Y.size() )
    {
      error( "trilinos::Matrix: Vector for matrix-vector result has wrong size" );
    }

    if ( size( 1 ) != X.size() )
    {
      error( "trilinos::Matrix: Non-matching dimensions %d and %d for matrix-vector product",
             size( 1 ), X.size() );
    }
  }
  else // transposed
  {
    if ( size( 0 ) != X.size() )
    {
      error( "trilinos::Matrix: Vector for transposed matrix-vector result has wrong size" );
    }

    if ( size( 1 ) != Y.size() )
    {
      error( "trilinos::Matrix: Non-matching dimensions %d and %d for transposed matrix-vector product",
             size( 1 ), Y.size() );
    }
  }


  mat_->apply( *X.vec(), *Y.vec(),
               ( transposed ? Teuchos::TRANS : Teuchos::NO_TRANS ) );
}

//-----------------------------------------------------------------------------

auto Matrix::operator*=( real a ) -> const Matrix &
{
  dolfin_assert( not mat_.is_null() );

  if ( mat_->isFillComplete() )
    mat_->resumeFill();

  mat_->scale( a );
  return *this;
}

//-----------------------------------------------------------------------------

auto Matrix::operator/=( real a ) -> const Matrix &
{
  dolfin_assert( not mat_.is_null() );

  if ( mat_->isFillComplete() )
    mat_->resumeFill();

  mat_->scale( 1.0 / a );
  return *this;
}

//-----------------------------------------------------------------------------

auto Matrix::operator=( GenericMatrix const & A ) -> const GenericMatrix &
{
  *this = A.down_cast< trilinos::Matrix const >();
  return *this;
}

//-----------------------------------------------------------------------------

auto Matrix::nz() const -> size_t
{
  // FIXME
  return DOLFIN_SIZE_T_MAX;
}

//-----------------------------------------------------------------------------

auto Matrix::factory() const -> LinearAlgebraFactory &
{
  return trilinos::Factory::instance();
}

//-----------------------------------------------------------------------------

auto Matrix::mat() const -> TPMatrixPtr
{
  return mat_;
}

//-----------------------------------------------------------------------------

auto Matrix::norm( const Norm type ) const -> real
{
  real norm = 0.0;

  switch ( type )
  {
    case l1:
      error( "trilinos::Matrix: unimplemented L1 norm." );
      break;
    case linf:
      error( "trilinos::Matrix: unimplemented inf norm." );
      break;
    case frobenius:
      norm = mat_->getFrobeniusNorm();
      break;
    default:
      error( "Unknown norm type." );
      break;
  }

  return norm;
}

//-----------------------------------------------------------------------------

auto Matrix::operator=( const Matrix & A ) -> const Matrix &
{
  // if ( this->size( 0 ) != A.size( 0 ) or this->size( 1 ) != A.size( 1 ) )
  {
    mat_ = Teuchos::RCP< TPMatrix >( new TPMatrix( A.mat_->getRowMap(),
                                                   A.mat_->getColMap(),
                                                   0 ) );
  }

  *mat_ = *A.mat_;

  return *this;
}

//-----------------------------------------------------------------------------

auto Matrix::operator+=( const Matrix & A ) -> const Matrix &
{
  dolfin_assert( not mat_.is_null() );

  trilinos::Matrix const & AA = A.down_cast< trilinos::Matrix const >();
  dolfin_assert( not AA.mat_.is_null() );

  mat_ = Teuchos::rcp_dynamic_cast< TPMatrix >( mat_->add( 1.0, *AA.mat_, 1.0,
                                                           Teuchos::null,
                                                           Teuchos::null,
                                                           Teuchos::null ) );

  return *this;
}

//-----------------------------------------------------------------------------

} // end namespace trilinos

} // end namespace dolfin

#endif // HAVE_TRILINOS
