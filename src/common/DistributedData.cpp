// Copyright (C) 2016 Aurelien Larcher.
// Licensed under the GNU LGPL Version 2.1.

#include <dolfin/common/DistributedData.h>

#include <dolfin/common/types.h>
#include <dolfin/common/AdjacentMapping.h>
#include <dolfin/common/GhostIterator.h>
#include <dolfin/common/OwnedIterator.h>
#include <dolfin/common/SharedIterator.h>
#include <dolfin/common/timing.h>
#include <dolfin/log/log.h>
#include <dolfin/main/MPI.h>

#include <string>

namespace dolfin
{

//-----------------------------------------------------------------------------

DistributedData::DistributedData( MPI::Communicator & comm )
  : Distributed< DistributedData >( comm )
  , rank_( Distributed::comm_rank() )
  , pe_size_( Distributed::comm_size() )
  , global_()
  , local_()
  , adjacents_()
  , shared_()
  , ghost_()
  , cached_numbering_()
  , cached_ownership_()
{
}

//-----------------------------------------------------------------------------

DistributedData::DistributedData( DistributedData const & other )
  : Distributed< DistributedData >( other )
  , valid_numbering( other.valid_numbering )
  , rank_( Distributed::comm_rank() )
  , pe_size_( Distributed::comm_size() )
  , range_is_set_( other.range_is_set_ )
  , offset_( other.offset_ )
  , range_size_( other.range_size_ )
  , global_size_( other.global_size_ )
  , finalized_( other.finalized_ )
  , global_( other.global_ )
  , local_( other.local_ )
  , adjacents_( other.adjacents_ )
  , shared_( other.shared_ )
  , ghost_( other.ghost_ )
  , cached_numbering_( other.cached_numbering_ )
  , cached_ownership_( other.cached_ownership_ )
  , shared_mapping_( ( other.shared_mapping_ == nullptr )
                       ? nullptr
                       : new SharedMapping( *other.shared_mapping_ ) )
{
}

//-----------------------------------------------------------------------------

DistributedData::~DistributedData()
{
  clear();
}

//-----------------------------------------------------------------------------

auto DistributedData::operator=( DistributedData const & other )
  -> DistributedData &
{
  DistributedData tmp( other );
  swap( *this, tmp );

  return *this;
}

//-----------------------------------------------------------------------------

void swap( DistributedData & a, DistributedData & b )
{
  using std::swap;

  swap( static_cast< Distributed< DistributedData > & >( a ),
        static_cast< Distributed< DistributedData > & >( b ) );

  // Swap flags
  swap( a.valid_numbering, b.valid_numbering );

  // Swap attributes
  swap( a.rank_, b.rank_ );
  swap( a.pe_size_, b.pe_size_ );
  swap( a.range_is_set_, b.range_is_set_ );
  swap( a.offset_, b.offset_ );
  swap( a.range_size_, b.range_size_ );
  swap( a.global_size_, b.global_size_ );
  swap( a.finalized_, b.finalized_ );
  swap( a.global_, b.global_ );
  swap( a.local_, b.local_ );
  swap( a.adjacents_, b.adjacents_ );
  swap( a.shared_, b.shared_ );
  swap( a.ghost_, b.ghost_ );
  swap( a.cached_numbering_, b.cached_numbering_ );
  swap( a.cached_ownership_, b.cached_ownership_ );
  swap( a.shared_mapping_, b.shared_mapping_ );
}

//-----------------------------------------------------------------------------

void DistributedData::clear()
{
  if ( shared_mapping_ )
  {
    delete shared_mapping_;
    shared_mapping_ = nullptr;
  }
  cached_ownership_ = {};
  cached_numbering_ = {};
  ghost_.clear();
  shared_.clear();
  adjacents_.clear();
  local_.clear();
  global_.clear();
  finalized_    = false;
  global_size_  = 0;
  range_size_   = 0;
  offset_       = 0;
  range_is_set_ = false;
  //
  valid_numbering = false;
}

//-----------------------------------------------------------------------------

void DistributedData::finalize()
{
  if ( finalized_ )
  {
    if ( local_.size() > 0 )
    {
      if ( cached_numbering_.empty() )
      {
        error(
          "DistributedData : data is finalized but empty numbering cache" );
      }
      if ( cached_ownership_.empty() )
      {
        error(
          "DistributedData : data is finalized but empty ownership cache" );
      }
    }
    // Be conservative for the moment
    error( "DistributedData : trying to finalize data twice" );
  }
  else
  {
    // message(1, "DistributedData : finalize");
    tic();

    // Check consistency
    if ( shared_.size() > local_.size() )
    {
      error( "DistributedData : shared size is greater than mapping size" );
    }
    if ( ghost_.size() > shared_.size() )
    {
      error( "DistributedData : ghost size is greater than shared size" );
    }

    /*
     *  Local-to-global mapping was provided and should be cached
     *
     */
    if ( cached_numbering_.empty() )
    {
      message(
        1, "DistributedData : cache local-to-global mapping (%u)", rank_ );

      // Generate cache for existing mapping
      if ( global_.size() != local_.size() )
      {
        error(
          "DistributedData : size mismatch between index mappings %u != %u",
          global_.size(),
          local_.size() );
      }

      // Cache numbering
      if ( global_.size() > 0 )
      {
        cached_numbering_ = std::vector< size_t >( global_.size() );
        for ( IndexMapping::value_type const & index_pair : global_ )
        {
          cached_numbering_[index_pair.first] = index_pair.second;
        }
        global_.clear();
      }
    }

    /*
     *  Set range and global size
     *
     */
    if ( !range_is_set_ )
    {
      if ( local_.size() < ghost_.size() )
      {
        error( "DistributedData : range not provided and empty mapping" );
      }

      // If the size has been provided initially then cache size was set,
      // otherwise the local-to-global mapping was just cached.
      size_t owned_size = cached_numbering_.size() - ghost_.size();

      // Set range, not recomputed if it is consistent
      set_range( owned_size, global_size_ );
    }

    /*
     * No mapping provided but a process range may have been provided or can be
     * inferred from the local size.
     * If *no* entities were marked as shared generate a linear mapping using
     * the process range, otherwise throw an error.
     * This allows automatic numbering of non-ghosted entities like cells.
     */
    if ( local_.size() == 0 )
    {
      if ( shared_.size() > 0 )
      {
        error( "DistributedData : no mapping was provided and some entities "
               "set as shared: impossible to devise a linear numbering." );
      }

      // Either local size or the range can be used but if both are provided
      // check consistency
      if ( range_size_ != cached_numbering_.size() )
      {
        error( "DistributedData : no ghost entries defined while size of local"
               "  size and range are not equal\n(local size) %u != %u (range)",
               cached_numbering_.size(), range_size_ );
      }

      // Numbering incrementally and set all as owned
      size_t global = offset_;
      for ( size_t local = 0; local < range_size_; ++local, ++global )
      {
        cached_numbering_[local] = global;
        local_[global]           = local;
      }

      // Global renumbering is not necessary
      valid_numbering = true;

      message( 1, "DistributedData : generated linear mapping in range [%u, %u["
                  " for rank %u", offset_, offset_ + range_size_, rank_ );
    }

    // At this point mappings exist and local-to-global is cached.
    // For the sake of completeness let us check the consistency
    if ( local_.size() != cached_numbering_.size() )
    {
      error( "DistributedData : size mismatch between local-to-global (%u) and "
             "and global-to-local (%u) mappings",
             cached_numbering_.size(), local_.size() );
    }

    // Cache ownership if needed
    if ( cached_ownership_.empty() && not cached_numbering_.empty() )
    {
      cached_ownership_ =
        std::vector< size_t >( cached_numbering_.size(), pe_size_ );

      // Update ownership for shared entities
      for ( SharedSet::const_iterator it = shared_.begin(); it != shared_.end();
            ++it )
      {
        cached_ownership_[it->first] = rank_;
      }

      // Update ownership for ghost entities
      for ( GhostSet::const_iterator it = ghost_.begin(); it != ghost_.end();
            ++it )
      {
        cached_ownership_[it->first] = it->second;
      }
    }

    //
    finalized_ = true;
    tocd( 1 );
  }
}

//-----------------------------------------------------------------------------

void DistributedData::assign( DistributedData const &       other,
                              std::vector< size_t > const & mapping )
{
  clear();

  if ( mapping.size() > other.local_size() )
  {
    error( "DistributedData : assignment requires mapping size (%u) lower than "
           "or equal to the local size of other distributed data (%u)",
           mapping.size(), other.local_size() );
  }

  cached_numbering_ = std::vector< size_t >( mapping.size() );
  cached_ownership_ = std::vector< size_t >( mapping.size() );

  // Extract numbering and ownership: two versions depending on the caching in
  // order to save map lookups
  if ( not other.cached_numbering_.empty()
       and not other.cached_ownership_.empty() )
  {
    SharedSet::const_iterator its;
    GhostSet::const_iterator  itg;
    for ( size_t i = 0; i < mapping.size(); ++i )
    {
      dolfin_assert( mapping[i] < other.cached_numbering_.size() );

      // Numbering
      size_t const global  = other.cached_numbering_[mapping[i]];
      cached_numbering_[i] = global;
      local_[global]       = i;

      dolfin_assert( mapping[i] < other.cached_numbering_.size() );
      size_t const owner = other.cached_ownership_[mapping[i]];

      // Ownership
      cached_ownership_[i] = other.cached_ownership_[mapping[i]];

      // Entity is shared: copy adjacents
      if ( owner < pe_size_ )
      {
        its = other.shared_.find( mapping[i] );
        dolfin_assert( its != other.shared_.end() );
        shared_[i] = its->second;
        adjacents_.insert( its->second.begin(), its->second.end() );

        // Entity is ghost: copy owner
        if ( owner != rank_ )
        {
          itg = other.ghost_.find( mapping[i] );
          dolfin_assert( itg != other.ghost_.end() );
          ghost_[i] = itg->second;
        }
      }
    }
  }
  else
  {
    IndexMapping::const_iterator it;
    SharedSet::const_iterator    its;
    GhostSet::const_iterator     itg;
    for ( size_t i = 0; i < mapping.size(); ++i )
    {
      // Numbering
      it                   = other.global_.find( mapping[i] );
      cached_numbering_[i] = it->second;
      local_[it->second]   = i;

      // Ownership
      its = other.shared_.find( mapping[i] );
      if ( its != other.shared_.end() )
      {
        shared_[i] = its->second;
        adjacents_.insert( shared_[i].begin(), shared_[i].end() );

        // Entity is ghost: copy owner
        itg = other.ghost_.find( mapping[i] );
        if ( itg != other.ghost_.end() )
        {
          ghost_[i]            = itg->second;
          cached_ownership_[i] = itg->second;
        }
        else
        {
          cached_ownership_[i] = rank_;
        }
      }
      else
      {
        cached_ownership_[i] = pe_size_;
      }
    }
  }

  ///
  range_size_ = cached_numbering_.size() - ghost_.size();
  MPI::offset( range_size_, offset_, this->comm() );
  MPI::all_reduce< MPI::sum >( range_size_, global_size_, this->comm() );

  ///
  finalized_ = true;
}

//-----------------------------------------------------------------------------

void DistributedData::set_range( size_t num_owned, size_t num_global /* = 0 */ )
{
  if ( finalized_ )
  {
    error( "DistributedData : setting range to a finalized data" );
  }
  // Check if there exists a mapping already
  if ( local_.size() > 0 && local_.size() < num_owned )
  {
    error( "DistributedData : provided range is greater than local size " );
  }
  // Check if the provided range size is consistent with cache size if any
  if ( not cached_numbering_.empty() and cached_numbering_.size() < num_owned )
  {
    error( "DistributedData : provided range is greater than cache size " );
  }
  // Check range if already set otherwise set it and compute offset
  if ( range_is_set_ )
  {
    if ( range_size_ != num_owned )
    {
      error( "DistributedData : setting different range than existing" );
    }
  }
  else
  {
    range_size_ = num_owned;
    MPI::offset( range_size_, offset_, this->comm() );
  }
  // Set the global size if provided otherwise compute it
  if ( num_global > 0 )
  {
    // Check if any existing global size matches the provided value
    if ( ( global_size_ > 0 ) && ( global_size_ != num_global ) )
    {
      error( "DistributedData : setting different global size than existing" );
    }
    global_size_ = num_global;
  }
  else
  {
    size_t range_sum;
    MPI::all_reduce< MPI::sum >( range_size_, range_sum, this->comm() );
    // Check that computed value matches the former value such that the sum of
    // ranges is indeed equal to the previously set global size
    if ( ( global_size_ > 0 ) && ( global_size_ != range_sum ) )
    {
      error( "DistributedData : sum of range not equal to global size" );
    }
    global_size_ = range_sum;
  }

  ///
  range_is_set_ = true;
}

//-----------------------------------------------------------------------------

void DistributedData::set_size( size_t num_local, size_t num_global /* = 0 */ )
{
  if ( finalized_ )
  {
    error( "DistributedData : setting size to a finalized data" );
  }
  // Do not allow setting the size of of a non-empty distributed data
  if ( !this->empty() )
  {
    error( "DistributedData : setting size to a non-empty data" );
  }
  // Do not allow resetting the size of data
  if ( not cached_numbering_.empty()
       and ( cached_numbering_.size() != num_local ) )
  {
    error( "DistributedData : setting different size to data" );
  }
  // If range is known check consistency, range is zero by default
  if ( num_local < range_size_ )
  {
    error( "DistributedData : setting smaller local size than existing range" );
  }
  // Set the global size if provided
  if ( num_global > 0 )
  {
    if ( ( global_size_ > 0 ) and ( global_size_ != num_global ) )
    {
      error( "DistributedData : setting different global size than existing" );
    }
    global_size_ = num_global;
  }
  // Use caching already as the data size is known and save on map lookups
  // Create arrays if they do not exist
  if ( cached_numbering_.empty() and ( num_local > 0 ) )
  {
    cached_numbering_ = std::vector< size_t >( num_local, DOLFIN_SIZE_T_UNDEF );
  }
  if ( cached_ownership_.empty() and ( num_local > 0 ) )
  {
    cached_ownership_ = std::vector< size_t >( num_local, pe_size_ );
  }
}

//-----------------------------------------------------------------------------

void DistributedData::set_map( size_t local_index,
                               size_t global_index,
                               bool   allow_remap /* = false */ )
{
  dolfin_assert( !finalized_ );
  dolfin_assert( local_.count( global_index ) == 0 );
  if ( not cached_numbering_.empty() )
  {
    dolfin_assert( local_index < cached_numbering_.size() );
    /* Do not allow remapping by default */
    if ( allow_remap && cached_numbering_[local_index] != DOLFIN_SIZE_T_UNDEF
         && cached_numbering_[local_index] != global_index )
    {
      // warning("DistributedData : remap %u", local_index);
      local_.erase( cached_numbering_[local_index] );
    }
    local_.insert( std::pair< size_t, size_t >( global_index, local_index ) );
    cached_numbering_[local_index] = global_index;
  }
  else
  {
    /* Do not allow remapping by default */
    if ( allow_remap )
    {
      IndexMapping::iterator it = global_.find( local_index );
      if ( it != global_.end() )
      {
        // warning("DistributedData : remap %u", local_index);
        local_.erase( it->second );
      }
    }
    local_.insert( std::pair< size_t, size_t >( global_index, local_index ) );
    dolfin_assert( global_.count( local_index ) == 0 );
    global_[local_index] = global_index;
  }
}

//-----------------------------------------------------------------------------

void DistributedData::set_map( std::vector< size_t > const & mapping )
{
  if ( finalized_ )
  {
    error( "DistributedData : setting numbering requires non-finalized data" );
  }
  if ( not cached_numbering_.empty() )
  {
    if ( mapping.size() != cached_numbering_.size() )
    {
      error(
        "DistributedData : local-to-global mapping array has invalid size" );
    }
  }
  else
  {
    if ( local_.size() && mapping.size() != global_.size() )
    {
      error(
        "DistributedData : local-to-global mapping array has invalid size" );
    }
    global_.clear();
    cached_numbering_ = std::vector< size_t >( mapping.size() );
  }

  std::copy( mapping.begin(), mapping.end(), cached_numbering_.begin() );
  local_.clear();
  for ( size_t i = 0; i < cached_numbering_.size(); ++i )
  {
    local_[cached_numbering_[i]] = i;
  }
}

//-----------------------------------------------------------------------------

void DistributedData::remap_numbering( std::vector< size_t > const & mapping )
{
  if ( !finalized_ )
  {
    error( "DistributedData : re-mapping numbering requires finalized data" );
  }

  if ( mapping.size() != cached_numbering_.size() )
  {
    error( "DistributedData : numbering re-mapping array has invalid size" );
  }

  // Update numbering
  dolfin_assert( global_.size() == 0 );
  dolfin_assert( not cached_numbering_.empty() );

  for ( IndexMapping::value_type & index_pair : local_ )
  {
    size_t const new_local_index = mapping[index_pair.second];
    // map new local index to global index
    cached_numbering_[new_local_index] = index_pair.first;
    // map global index to new local index
    index_pair.second = new_local_index;
  }

  // Update shared entities
  dolfin_assert( not cached_ownership_.empty() );
  std::fill( cached_ownership_.begin(), cached_ownership_.end(), pe_size_ );
  SharedSet shared;
  for ( SharedSet::const_iterator it = shared_.begin(); it != shared_.end();
        ++it )
  {
    size_t const new_local_index = mapping[it->first];
    // map new local index to shared adjacents
    shared[new_local_index] = it->second;
    // set default ownership to new local index
    cached_ownership_[new_local_index] = rank_;
  }
  shared_.swap( shared );

  // Update ghost entities
  dolfin_assert( not cached_ownership_.empty() );
  GhostSet ghost;
  for ( GhostSet::const_iterator it = ghost_.begin(); it != ghost_.end(); ++it )
  {
    size_t const new_local_index = mapping[it->first];
    // map new local index to owner
    ghost[new_local_index] = it->second;
    // set ghost ownership to new local index
    cached_ownership_[new_local_index] = it->second;
  }
  ghost_.swap( ghost );

  // Clear mappings
  if ( shared_mapping_ )
  {
    delete shared_mapping_;
    shared_mapping_ = nullptr;
  }
}

//-----------------------------------------------------------------------------

void DistributedData::renumber_global()
{
  if ( not finalized_ )
  {
    error( "DistributedData : global renumbering requires finalized data" );
  }

  /*
   * The following code assumes that numbering and ownership are finalized !
   */

#if DOLFIN_HAVE_MPI

  message( 1,
           "DistributedData : renumber global, local size = %u",
           cached_numbering_.size() );
  tic();

  IndexMapping                         local_mapping;
  std::vector< std::vector< size_t > > sendbuf( pe_size_ );

  // Re-index owned entities and collect ghosted entities per owner
  dolfin_assert( local_.size() == cached_numbering_.size() );
  dolfin_assert( !( local_.size() > 0 && cached_numbering_.empty() ) );
  dolfin_assert( !( local_.size() > 0 && cached_ownership_.empty() ) );
  size_t index = offset_;
  for ( size_t i = 0; i < cached_numbering_.size(); ++i )
  {
    dolfin_assert( cached_ownership_[i] <= pe_size_ );
    if ( cached_ownership_[i] == pe_size_ || cached_ownership_[i] == rank_ )
    {
      cached_numbering_[i] = index;
      local_mapping[index] = i;
      ++index;
    }
    else
    {
      dolfin_assert( cached_ownership_[i] < pe_size_ );
      sendbuf[cached_ownership_[i]].push_back( cached_numbering_[i] );
    }
  }

  // Exchange data and set numbering

  // Maximum number of received entities is the number of owned shared but the
  // issue is that some previous were written in a way that does not ensure
  // appropriate setting of owned shared entities
  size_t recvsize = max_array_size( sendbuf );
  MPI::all_reduce< MPI::max >( recvsize, recvsize, comm() );

  std::vector< size_t > recvbuf( recvsize );
  std::vector< size_t > sendbck( recvsize );
  std::vector< size_t > recvbck( ghost_.size() );

  for ( size_t j = 1; j < pe_size_; ++j )
  {
    int src = ( rank_ - j + pe_size_ ) % pe_size_;
    int dst = ( rank_ + j ) % pe_size_;

    int recv_count =
      MPI::sendrecv( sendbuf[dst], dst, recvbuf, src, 1, comm() );
    sendbck.resize( recv_count );

    for ( int k = 0; k < recv_count; ++k )
    {
      dolfin_assert( local_.count( recvbuf[k] ) > 0 );
      size_t const local_index = local_.find( recvbuf[k] )->second;
      sendbck[k]               = cached_numbering_[local_index];

      // Entity is not marked as shared: invalidate ownership
      if ( cached_ownership_[local_index] == pe_size_ )
      {
        // error("Entity %u is not marked as shared", local_index);
      }
      else if ( cached_ownership_[local_index] != rank_ )
      {
        error( "DistributedData : entity %u should be owned but is set ghost" );
      }
    }

    recvbck.resize( sendbuf[dst].size() );
    MPI::sendrecv( sendbck, src, recvbck, dst, 2, comm() );

    for ( size_t k = 0; k < recvbck.size(); ++k )
    {
      size_t const local_index = local_.find( sendbuf[dst][k] )->second;
      dolfin_assert( local_mapping.count( recvbck[k] ) == 0 );
      cached_numbering_[local_index] = recvbck[k];
      local_mapping[recvbck[k]]      = local_index;
    }
  }

  local_.swap( local_mapping );

  tocd( 1 );

#endif /* DOLFIN_HAVE_MPI */
}

//-----------------------------------------------------------------------------

void DistributedData::remap_ownership( std::vector< size_t > const & mapping )
{
  dolfin_assert( finalized_ );
  if ( mapping.size() != pe_size_ )
  {
    error( "DistributedData : ownership re-mapping array has invalid size" );
  }

  // Update current rank
  rank_ = mapping[rank_];

  // Update adjacent ranks
  _set< size_t > adjs;
  for ( _set< size_t >::const_iterator it = adjacents_.begin();
        it != adjacents_.end();
        ++it )
  {
    adjs.insert( mapping[*it] );
  }
  adjacents_ = adjs;

  // Update shared entities ownership
  for ( SharedSet::value_type & index_pair : shared_ )
  {
    // Update adjacent
    _set< size_t > adj;
    for ( size_t const & id : index_pair.second )
      adj.insert( mapping[id] );

    index_pair.second = adj;

    // Update cached owner
    if ( not cached_ownership_.empty() )
    {
      cached_ownership_[index_pair.first] = rank_;
    }
  }

  // Update ghost entities ownership
  for ( GhostSet::value_type & index_pair : local_ )
  {
    // Update owner
    index_pair.second = mapping[index_pair.second];

    // Update cached owner
    if ( not cached_ownership_.empty() )
    {
      cached_ownership_[index_pair.first] = mapping[index_pair.second];
    }
  }

  // Clear mappings
  // TODO: implement rank re-mapping in adjacent mappings
  if ( shared_mapping_ )
  {
    delete shared_mapping_;
    shared_mapping_ = nullptr;
  }
}

//-----------------------------------------------------------------------------

void DistributedData::get_common_adj( size_t                        n,
                                      std::vector< size_t > const & indices,
                                      _set< size_t > &              adjs ) const
{
  if ( n == 0 )
  {
    adjs.clear();
    return;
  }
  dolfin_assert( shared_.count( indices[0] ) > 0 );
  adjs = shared_.find( indices[0] )->second;
  for ( size_t i = 1; ( adjs.size() > 0 ) && ( i < n ); ++i )
  {
    dolfin_assert( shared_.count( indices[i] ) > 0 );
    _set< size_t > const & adjx = shared_.find( indices[i] )->second;
    for ( _set< size_t >::iterator it = adjs.begin(); it != adjs.end(); )
    {
      if ( adjx.count( *it ) == 0 )
      {
        adjs.erase( it++ );
      }
      else
      {
        ++it;
      }
    }
  }
}

//-----------------------------------------------------------------------------

void DistributedData::set_shared( size_t local_index )
{
  dolfin_assert( !finalized_ );
  dolfin_assert( shared_.count( local_index ) == 0 );
  dolfin_assert( !( ( not cached_ownership_.empty() )
                    && ( local_index >= cached_ownership_.size() ) ) );
  if ( ( not cached_ownership_.empty() )
       && ( cached_ownership_[local_index] == pe_size_ ) )
  {
    cached_ownership_[local_index] = rank_;
  }
  // As explained we allow setting an entity as shared without adjacent only if
  // the entity is not shared already.
  dolfin_assert( !( ( cached_ownership_.empty() )
                    && ( global_.count( local_index ) == 0 ) ) );
  if ( shared_[local_index].size() > 0 )
  {
    error( "DistributedData : cannot set_shared on entities with adjacents" );
  }
}

//-----------------------------------------------------------------------------

void DistributedData::set_shared_adj( size_t local_index, size_t adj )
{
  dolfin_assert( !finalized_ );
  dolfin_assert( adj != rank_ );
  dolfin_assert( adj < pe_size_ );
  dolfin_assert( !( ( not cached_ownership_.empty() )
                    && ( local_index >= cached_ownership_.size() ) ) );
  if ( ( not cached_ownership_.empty() )
       && ( cached_ownership_[local_index] == pe_size_ ) )
  {
    cached_ownership_[local_index] = rank_;
  }
  shared_[local_index].insert( adj );
  adjacents_.insert( adj );
}

//-----------------------------------------------------------------------------

void DistributedData::setall_shared_adj( size_t                 local_index,
                                         _set< size_t > const & adjs )
{
  dolfin_assert( !finalized_ );
  dolfin_assert( adjs.count( rank_ ) == 0 );
  dolfin_assert( !( ( not cached_ownership_.empty() )
                    && ( local_index >= cached_ownership_.size() ) ) );
  if ( ( not cached_ownership_.empty() )
       && ( cached_ownership_[local_index] == pe_size_ ) )
  {
    cached_ownership_[local_index] = rank_;
  }
  shared_[local_index] = adjs;
  adjacents_.insert( adjs.begin(), adjs.end() );
}

//-----------------------------------------------------------------------------

auto DistributedData::shared_mapping() const -> SharedMapping const &
{
  if ( shared_mapping_ == nullptr )
  {
    shared_mapping_ = new SharedMapping( *this );
  }
  return *shared_mapping_;
}

//-----------------------------------------------------------------------------

void DistributedData::remap_shared_adj()
{
#if DOLFIN_HAVE_MPI
  size_t const pe_rank = this->comm_rank();
  size_t const pe_size = this->comm_size();

  std::vector< size_t > buffer;
  for ( SharedIterator vi( *this ); vi.valid(); ++vi )
  {
    buffer.push_back( vi.global_index() );
  }

  size_t recvmax = buffer.size();
  MPI::all_reduce_in_place< MPI::max >( recvmax, comm() );
  std::vector< size_t > recvbuf( recvmax );
  for ( size_t j = 1; j < pe_size; ++j )
  {
    int src = ( pe_rank - j + pe_size ) % pe_size;
    int dst = ( pe_rank + j ) % pe_size;

    int recvcount = MPI::sendrecv( buffer, dst, recvbuf, src, 0, comm() );

    for ( int k = 0; k < recvcount; ++k )
    {
      if ( this->has_global( recvbuf[k] ) )
      {
        size_t const local_index = this->get_local( recvbuf[k] );
        this->set_shared_adj( local_index, src );
      }
    }
  }
#endif /* DOLFIN_HAVE_MPI */
}

//-----------------------------------------------------------------------------

void DistributedData::set_ghost( size_t local_index, size_t owner )
{
  dolfin_assert( !finalized_ );
  dolfin_assert( owner != rank_ );
  dolfin_assert( owner < pe_size_ );
  if ( not cached_ownership_.empty() )
  {
    dolfin_assert( local_index < cached_ownership_.size() );
    cached_ownership_[local_index] = owner;
  }
  shared_[local_index].insert( owner );
  ghost_[local_index] = owner;
  adjacents_.insert( owner );
}

//-----------------------------------------------------------------------------

void DistributedData::disp() const
{
  section( "DistributedData" );
  message( "rank        : %8u", rank_ );
  message( "offset      : %8u", offset_ );
  message( "range size  : %8u", range_size_ );
  message( "local  size : %8u", this->local_size() );
  message( "global size : %8u", this->global_size() );
  message( "cached      : %8u", ( not cached_numbering_.empty() ) );
  message( "finalized   : %8u", finalized_ );
  /*
  if (finalized_)
  {
    section("entities    :");
    for (size_t i = 0; i < this->local_size(); ++i)
    {
      message("%8u -> %8u -> %8u : %8u", i, cached_numbering_[i],
              local_.find(cached_numbering_[i])->second, this->get_owner(i));
    }
    end();
  }
  */
  end();
}

//-----------------------------------------------------------------------------

void DistributedData::check_shared()
{
#if DOLFIN_HAVE_MPI
  size_t const pe_rank = this->comm_rank();
  size_t const pe_size = this->comm_size();

  std::vector< std::vector< size_t > > buffer( pe_size );
  for ( SharedIterator vi( *this ); vi.valid(); ++vi )
  {
    dolfin_assert( !vi.adj().empty() );
    vi.adj_enqueue( buffer, vi.global_index() );
  }

  //
  std::vector< size_t > recvbuf( this->num_shared() );
  for ( size_t j = 1; j < pe_size; ++j )
  {
    int src = ( pe_rank - j + pe_size ) % pe_size;
    int dst = ( pe_rank + j ) % pe_size;

    int recvcount = MPI::sendrecv( buffer[dst], dst, recvbuf, src, 0, comm() );

    for ( int k = 0; k < recvcount; ++k )
    {
      if ( !this->has_global( recvbuf[k] ) )
      {
        error( "Shared entity not found on adjacent rank" );
      }
      size_t const local_index = this->get_local( recvbuf[k] );
      if ( !this->is_shared( local_index ) )
      {
        error( "Shared entity not marked as shared" );
      }
      if ( this->get_shared_adj( local_index ).count( src ) == 0 )
      {
        error( "Shared entity not marked as shared with rank %u", src );
      }
    }
  }
#endif /* DOLFIN_HAVE_MPI */
}

//-----------------------------------------------------------------------------

void DistributedData::check_ghost()
{
#if DOLFIN_HAVE_MPI
  size_t const pe_rank = this->comm_rank();
  size_t const pe_size = this->comm_size();

  std::vector< std::vector< size_t > > buffer( pe_size );
  for ( GhostIterator vi( *this ); vi.valid(); ++vi )
  {
    dolfin_assert( !vi.adj().empty() );
    if ( vi.owner() == pe_rank )
    {
      error( "Ghost entity is marked as owned" );
    }
    buffer[vi.owner()].push_back( vi.global_index() );
  }

  //
  size_t recvmax = 0;
  for ( size_t j = 0; j < pe_size; ++j )
  {
    size_t s = buffer[j].size();
    MPI::reduce< MPI::sum >( &s, &recvmax, 1, j, comm() );
  }
  std::vector< size_t > recvbuf( recvmax );
  for ( size_t j = 1; j < pe_size; ++j )
  {
    int src = ( pe_rank - j + pe_size ) % pe_size;
    int dst = ( pe_rank + j ) % pe_size;

    int recvcount = MPI::sendrecv( buffer[dst], dst, recvbuf, src, 0, comm() );

    for ( int k = 0; k < recvcount; ++k )
    {
      if ( !this->has_global( recvbuf[k] ) )
      {
        error( "Shared entity not found on owner rank" );
      }
      size_t const local_index = this->get_local( recvbuf[k] );
      if ( !this->is_shared( local_index ) )
      {
        error( "Shared entity not marked as shared" );
      }
      if ( this->is_ghost( local_index ) )
      {
        error( "Shared owned entity is marked as ghost" );
      }
      if ( this->get_shared_adj( local_index ).count( src ) == 0 )
      {
        error( "Shared entity not marked as shared with rank %u", src );
      }
    }
  }
#endif /* DOLFIN_HAVE_MPI */
}

//-----------------------------------------------------------------------------

} /* namespace dolfin */
