// Copyright (C) 2013-2014 Aurélien Larcher
// Licensed under the GNU LGPL Version 2.1.
//

#include <dolfin/fem/DofMap.h>
#include <dolfin/fem/DofMapCache.h>
#include <dolfin/fem/Form.h>
#include <dolfin/function/Function.h>

#include <iomanip>

namespace dolfin
{

//-----------------------------------------------------------------------------

DofMapCache::DofMapCache()
{
  cache_.clear();
  rlist_.clear();
}

//-----------------------------------------------------------------------------

DofMapCache::~DofMapCache()
{
  if ( cache_.size() != 0 )
  {
    warning( "DofMapCache is not empty: "
             "some dof maps have not been properly released" );
  }
  for ( container_t::value_type & token : cache_ )
  {
    delete token.second.item;
  }
  cache_.clear();
}

//-----------------------------------------------------------------------------

auto DofMapCache::acquire( Mesh & mesh, Form const & form, size_t const & i )
  -> DofMap &
{
  dolfin_assert( not form.dofmaps().empty() );

  // Create UFC dof map for the i-th coefficient to get the signature
  ufc::dofmap * ufc_dofmap = form().create_dofmap( i );

  // Do not transfer ownership to the possibly created dofmap
  // The UFC dofmap is cloned as member attribute of the DOLFIN dofmap
  DofMap * ret = &acquire( mesh, *ufc_dofmap, false );

  // Delete UFC dof map as it is not longer used
  delete ufc_dofmap;

  return *ret;
}

//-----------------------------------------------------------------------------

auto DofMapCache::acquire( Mesh & mesh, ufc::dofmap const & dofmap, bool owner )
  -> DofMap &
{
  DofMap *              ret = nullptr;
  std::string const     h   = DofMap::make_hash( mesh, dofmap );
  container_t::iterator it  = cache_.find( h );

  if ( it == cache_.end() )
  {
    // Create DOLFIN dofmap and insert in map
    ret = new DofMap( mesh, dofmap );
    cache_.insert( item_t( h, token_t( ret ) ) );

    if ( rlist_.find( ret ) == rlist_.end() )
    {
      rlist_.insert( ritem_t( ret, h ) );
    }
    else
    {
      error( "DofMap pointer has already been registered with another hash" );
    }
  }
  else
  {
    // The dofmap has already been created
    ret = it->second.item;
    dolfin_assert( ret != nullptr );

    // Check hash consistency
    std::string const dm_h = ret->hash();
    if ( h != dm_h )
    {
      disp();
      error("DofMap@%p\n"
            "Signature to be inserted  : %s\n"
            "Signature of DofMap       : %s\n"
            "DofMap object refers to two different signatures",
            ret, h.c_str(), dm_h.c_str() );
    }
    it->second.count++;

    // Ownership was transfered but the dofmap already exists, the UFC dofmap
    // passed as argument will not be used.
    if ( owner )
    {
      delete &dofmap;
    }
  }
  return *ret;
}

//-----------------------------------------------------------------------------

void DofMapCache::release( DofMap & dofmap )
{
  std::string const h          = dofmap.hash();
  rlist_t::iterator it         = rlist_.find( &dofmap );
  std::string const expected_h = it->second;

  if ( it == rlist_.end() )
  {
    error( "Trying to release inexistent DofMap (%s)", h.c_str() );
  }
  else
  {
    if ( h != expected_h )
    {
      disp();
      std::stringstream ss;
      error("DofMap@%p\n"
            "Signature to be inserted  : %s\n"
            "Signature of DofMap       : %s\n"
            "DofMap object refers to two different signatures",
            &dofmap, h.c_str(), expected_h.c_str() );
    }
    container_t::iterator dm_it    = cache_.find( expected_h );
    token_t &             dm_token = dm_it->second;
    dm_token.count--;

    if ( dm_token.count == 0 )
    {
      delete dm_token.item;
      rlist_.erase( it );
      cache_.erase( dm_it );
    }
  }
}

//-----------------------------------------------------------------------------

void DofMapCache::disp() const
{
  message( "Number of DofMaps in cache : %i", cache_.size() );
  message( "Cache :" );

  for ( container_t::value_type const & token : cache_ )
  {
    std::cout << std::setw( 128 ) << token.first << " : @" << token.second.item
              << " : " << token.second.count << std::endl;
  }
  message( "Reverse list :" );
  for ( rlist_t::value_type const & rlist : rlist_ )
  {
    std::cout << "@" << rlist.first << " : " << rlist.second << std::endl;
  }
}

//-----------------------------------------------------------------------------

} // namespace dolfin
