#ifndef __DOLFIN_HEADER_QUADRATURE_H
#define __DOLFIN_HEADER_QUADRATURE_H

// DOLFIN quadrature interface

#include <dolfin/quadrature/Quadrature.h>
#include <dolfin/quadrature/GaussianQuadrature.h>
#include <dolfin/quadrature/GaussQuadrature.h>
#include <dolfin/quadrature/RadauQuadrature.h>
#include <dolfin/quadrature/LobattoQuadrature.h>
#include <dolfin/quadrature/QuadratureRule.h>
#include <dolfin/quadrature/GaussQuadratureRule.h>
#include <dolfin/quadrature/GaussTensorQuadratureRule.h>

#endif /* __DOLFIN_HEADER_QUADRATURE_H */
