// Copyright (C) 2008 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
// This file provides DOLFIN typedefs for basic types.

#ifndef __DOLFIN_TYPES_H
#define __DOLFIN_TYPES_H

#include <dolfin/config/dolfin_config.h>

#if HAVE_PARALLEL_HASH_MAP
#include <parallel_hashmap/phmap.h>
#elif __sgi
#include <hash_map>
#include <hash_set>
#else
#include <unordered_map>
#include <unordered_set>
#endif

#include <algorithm>
#include <cfloat>
#include <complex>
#include <cstdint>
#include <limits>
#include <map>
#include <set>
#include <vector>

namespace dolfin
{

// Real numbers
using real = double;

// Unsigned integers
using uint   = unsigned int;
using size_t = std::size_t;

// Index type (at least 64bit)
using uidx = uint64_t;

// Complex numbers
using complex = std::complex< double >;

//-----------------------------------------------------------------------------

constexpr real DOLFIN_REAL_MIN   = std::numeric_limits< real >::lowest();
constexpr real DOLFIN_REAL_MAX   = std::numeric_limits< real >::max();
constexpr real DOLFIN_REAL_UNDEF = std::numeric_limits< real >::max();

constexpr int DOLFIN_INT_MIN   = std::numeric_limits< int >::lowest();
constexpr int DOLFIN_INT_MAX   = std::numeric_limits< int >::max();
constexpr int DOLFIN_INT_UNDEF = std::numeric_limits< int >::max();

constexpr long DOLFIN_LONG_MIN   = std::numeric_limits< long >::lowest();
constexpr long DOLFIN_LONG_MAX   = std::numeric_limits< long >::max();
constexpr long DOLFIN_LONG_UNDEF = std::numeric_limits< long >::max();

constexpr uint DOLFIN_UINT_MIN   = std::numeric_limits< uint >::lowest();
constexpr uint DOLFIN_UINT_MAX   = std::numeric_limits< uint >::max();
constexpr uint DOLFIN_UINT_UNDEF = std::numeric_limits< uint >::max();

constexpr size_t DOLFIN_SIZE_T_MIN   = std::numeric_limits< size_t >::lowest();
constexpr size_t DOLFIN_SIZE_T_MAX   = std::numeric_limits< size_t >::max();
constexpr size_t DOLFIN_SIZE_T_UNDEF = std::numeric_limits< size_t >::max();

//-----------------------------------------------------------------------------

template < typename Key,
           typename Value,
           typename Compare = std::less< Key >,
           typename Allocator =
             std::allocator< std::pair< const Key, Value > > >
using _ordered_map = std::map< Key, Value, Compare, Allocator >;

template < typename Key,
           typename Compare   = std::less< Key >,
           typename Allocator = std::allocator< Key > >
using _ordered_set = std::set< Key, Compare, Allocator >;

#if HAVE_PARALLEL_HASH_MAP

template < typename Key,
           typename Value,
           typename Hash  = phmap::priv::hash_default_hash< Key >,
           typename Eq    = phmap::priv::hash_default_eq< Key >,
           typename Alloc = std::allocator< std::pair< const Key, Value > > >
using _map = phmap::flat_hash_map< Key, Value, Hash, Eq, Alloc >;

template < typename Key,
           typename Hash  = phmap::priv::hash_default_hash< Key >,
           typename Eq    = phmap::priv::hash_default_eq< Key >,
           typename Alloc = std::allocator< Key > >
using _set = phmap::flat_hash_set< Key, Hash, Eq, Alloc >;

#elif __sgi

template < typename Key,
           typename Value,
           typename Hash  = std::hash< Key >,
           typename Comp  = std::equal_to< Key >,
           typename Alloc = std::allocator< std::pair< const Key, Value > > >
using _map = std::hash_map< Key, Value, Hash, Comp, Alloc >;

template < typename Key,
           typename Hash  = std::hash< Key >,
           typename Comp  = std::equal_to< Key >,
           typename Alloc = std::allocator< Key > >
using _set = std::hash_set< Key, Hash, Comp, Alloc >;

#else

template < typename Key,
           typename Value,
           typename Hash  = std::hash< Key >,
           typename Comp  = std::equal_to< Key >,
           typename Alloc = std::allocator< std::pair< const Key, Value > > >
using _map = std::unordered_map< Key, Value, Hash, Comp, Alloc >;

template < typename Key,
           typename Hash  = std::hash< Key >,
           typename Comp  = std::equal_to< Key >,
           typename Alloc = std::allocator< Key > >
using _set = std::unordered_set< Key, Hash, Comp, Alloc >;

#endif

//-----------------------------------------------------------------------------

template < typename T >
void destruct( std::vector< T * > & objects )
{
  for ( T * element : objects )
    if ( element != nullptr )
      delete element;

  objects.clear();
}

//-----------------------------------------------------------------------------

template < typename T, typename Iterator >
inline void append( std::vector< T > & array, Iterator begin, Iterator end )
{
#ifdef __SUNPRO_CC
  for ( Iterator it = begin; it != end; ++it )
  {
    array.push_back( *it );
  }
#else
  array.insert( array.end(), begin, end );
#endif
}

//-----------------------------------------------------------------------------

template < typename T >
inline auto max_array_size( std::vector< std::vector< T > > const & arrays )
  -> size_t
{
  return std::max_element( arrays.begin(), arrays.end(),
           []( std::vector< T > const & a, std::vector< T > const & b ) {
             return a.size() < b.size();
           } )->size();
}

//-----------------------------------------------------------------------------

/// Facility to compare object through pointers
template < class T >
auto objptrcmp( T const * p0, T const * p1 ) -> bool
{
  if ( p0 == p1 )
  {
    return true;
  }
  else if ( ( p0 == nullptr && p1 != nullptr )
            || ( p0 != nullptr && p1 == nullptr ) )
  {
    return false;
  }
  return ( *p0 == *p1 );
}

//-----------------------------------------------------------------------------

} // end namespace dolfin

#endif
