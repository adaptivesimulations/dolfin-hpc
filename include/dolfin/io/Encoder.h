// Copyright (C) 2009 Garth N. Wells.
// Licensed under the GNU LGPL Version 2.1.

#ifndef __DOLFIN_ENCODER_H
#define __DOLFIN_ENCODER_H

#include <dolfin/config/dolfin_config.h>

#include <dolfin/common/types.h>
#include <dolfin/io/base64.h>

#ifdef HAVE_LIBZ
#include <zlib.h>
extern "C"
{
  auto compress( Bytef *       dest,
                 uLongf *      destLen,
                 const Bytef * source,
                 uLong         sourceLen ) -> int;
}
#endif

#include <sstream>
#include <utility>
#include <vector>

namespace dolfin
{

/// Thes functions class provide tools for encoding and compressing streams
/// for use in output files

/// We cheating in some functions by relying on std::vector data being
/// contiguous in memory. This will be part of the upcoming C++ standard.
/// COMMENT: Only if 2009 happened before the C++2003 Addendum.

namespace Encoder
{

//----------------------------------------------------------------------------

template < typename T >
static void encode_base64( const T *           data,
                           size_t              length,
                           std::stringstream & encoded_data )
{
  encoded_data << base64_encode( ( const unsigned char * ) &data[0],
                                 length * sizeof( T ) );
}

//----------------------------------------------------------------------------

template < typename T >
static void encode_base64( const std::vector< T > & data,
                           std::stringstream &      encoded_data )
{
  encoded_data << base64_encode( ( const unsigned char * ) &data[0],
                                 data.size() * sizeof( T ) );
}

//----------------------------------------------------------------------------

#ifdef HAVE_LIBZ
template < typename T >
static auto compress_data( const std::vector< T > & data )
  -> std::pair< unsigned char *, unsigned long >
{
  // Compute length of uncompressed data
  const unsigned long uncompressed_size = data.size() * sizeof( T );

  // Compute maxium length of compressed data
  unsigned long compressed_size =
    ( uncompressed_size + ( ( ( uncompressed_size ) / 1000 ) + 1 ) + 12 );
  ;

  // Allocate space for compressed data
  unsigned char * compressed_data = new unsigned char[compressed_size];

  // Compress data
  if ( compress( ( Bytef * ) compressed_data,
                 &compressed_size,
                 ( const Bytef * ) &data[0],
                 uncompressed_size )
       != Z_OK )
    error( "Zlib error while compressing data." );

  // Make pair and return
  return std::make_pair( compressed_data, compressed_size );
}
#endif

//----------------------------------------------------------------------------

} // namespace Encoder

//----------------------------------------------------------------------------

} // namespace dolfin

#endif
