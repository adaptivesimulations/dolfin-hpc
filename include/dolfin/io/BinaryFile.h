// Copyright (C) 2009-2015 Niclas Jansson.
// Licensed under the GNU LGPL Version 2.1.

#ifndef __DOLFIN_BINARY_FILE_H
#define __DOLFIN_BINARY_FILE_H

#include <dolfin/io/GenericFile.h>
#include <dolfin/mesh/celltypes/CellType.h>

#include <string>

#define BINARY_MAGIC_V1 0xBABE
#define BINARY_MAGIC_V2 0xB4B3
#define BINARY_MAGIC BINARY_MAGIC_V2
#define FNAME_LENGTH 256
#define BINARY_VERSION 2

namespace dolfin
{

class Vector;

class BinaryFile : public GenericFile
{

public:
  ///
  BinaryFile( const std::string filename );

  ///
  BinaryFile( const std::string filename, real const & t );

  ///
  ~BinaryFile() override = default;

  /// Input
  void operator>>( GenericVector & x ) override;
  void operator>>( Mesh & mesh ) override;
  void operator>>( Function & f ) override;
  void operator>>( LabelList< Function > & f ) override;
  void operator>>( MeshFunction< bool > & meshfunction ) override;
  void operator>>( MeshFunction< int > & meshfunction ) override;
  void operator>>( MeshFunction< size_t > & meshfunction ) override;
  void operator>>( MeshFunction< real > & meshfunction ) override;

  /// Output
  void operator<<( GenericVector & x ) override;
  void operator<<( Mesh & mesh ) override;
  void operator<<( Function & u ) override;
  void operator<<( LabelList< Function > & f ) override;
  void operator<<( MeshFunction< bool > & meshfunction ) override;
  void operator<<( MeshFunction< int > & meshfunction ) override;
  void operator<<( MeshFunction< size_t > & meshfunction ) override;
  void operator<<( MeshFunction< real > & meshfunction ) override;

  /// Overload GenericFile
  void read() override;
  void write() override;

  enum Binary_data_t
  {
    BINARY_MESH_DATA,
    BINARY_VECTOR_DATA,
    BINARY_FUNCTION_DATA,
    BINARY_MESH_FUNCTION_DATA
  };

  using BinaryFileHeader = struct
  {
    uint32_t      magic;
    uint32_t      bendian;
    uint32_t      pe_size;
    Binary_data_t type;
  };

#ifdef ENABLE_MPIIO
  using BinaryFunctionHeader = struct
  {
    uint32_t dim;
    uint32_t size;
    real     t;
    char     name[FNAME_LENGTH];
  };
#endif

private:
  template < typename T >
  void write_meshfunction( MeshFunction< T > & meshfunction );

  template < class T >
  void read_meshfunction( MeshFunction< T > & meshfunction );

  void nameUpdate( const int counter );

  void write_function( LabelList< Function > & f );

  auto hdr_check( BinaryFileHeader & hdr, Binary_data_t type, size_t pe_size )
    -> bool;

#ifdef ENABLE_MPIIO
  void bswap_func_hdr( BinaryFunctionHeader & hdr );
#endif

  /// Returns binary file cell type identifier for given DOLFIN cell type
  auto cell_type( size_t version, CellType::Type const type ) -> size_t;

  /// Returns DOLFIN cell type for given binary file cell type identifier
  auto cell_type( size_t version, size_t const type ) -> CellType::Type;

  // Function filename
  std::string bin_filename_;

  // Current time
#if defined( DOLFIN_HAVE_MPI )
  real const * const t_;
#endif

  // Version number of the binary file
  size_t version_;
};

} /* namespace dolfin */

#endif /* __DOLFIN_BINARY_FILE_H */
