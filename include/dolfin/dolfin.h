#ifndef __DOLFIN_H
#define __DOLFIN_H

// DOLFIN interface

#include <dolfin/adaptivity/dolfin_adaptivity.h>
#include <dolfin/common/dolfin_common.h>
#include <dolfin/parameter/dolfin_parameter.h>
#include <dolfin/log/dolfin_log.h>
#include <dolfin/la/dolfin_la.h>
#include <dolfin/evolution/dolfin_evolution.h>
#include <dolfin/function/dolfin_function.h>
#include <dolfin/io/dolfin_io.h>
#include <dolfin/insitu/dolfin_insitu.h>
#include <dolfin/main/dolfin_main.h>
#include <dolfin/math/dolfin_math.h>
#include <dolfin/mesh/dolfin_mesh.h>
#include <dolfin/fem/dolfin_fem.h>
#include <dolfin/quadrature/dolfin_quadrature.h>

#endif
