#ifndef __DOLFIN_HEADER_PARAMETER_H
#define __DOLFIN_HEADER_PARAMETER_H

/// DOLFIN parameter interface

#include <dolfin/parameter/Parameter.h>
#include <dolfin/parameter/ParameterSystem.h>
#include <dolfin/parameter/parameters.h>

#endif /* __DOLFIN_HEADER_PARAMETER_H */
