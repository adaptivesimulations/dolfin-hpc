#ifndef __DOLFIN_HEADER_FEM_H
#define __DOLFIN_HEADER_FEM_H

// DOLFIN fem interface

#include <dolfin/fem/Assembler.h>
#include <dolfin/fem/BilinearForm.h>
#include <dolfin/fem/BoundaryCondition.h>
#include <dolfin/fem/CoefficientMap.h>
#include <dolfin/fem/DirichletBC.h>
#include <dolfin/fem/DofMap.h>
#include <dolfin/fem/Elements.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/fem/FiniteElementSpace.h>
#include <dolfin/fem/Form.h>
#include <dolfin/fem/Functional.h>
#include <dolfin/fem/LinearForm.h>
#include <dolfin/fem/NodeNormal.h>
#include <dolfin/fem/PeriodicBC.h>
#include <dolfin/fem/SlipBC.h>
#include <dolfin/fem/SlipFrictionBC.h>
#include <dolfin/fem/SubSystem.h>

#endif /* __DOLFIN_HEADER_FEM_H */
