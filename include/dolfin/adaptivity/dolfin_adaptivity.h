#ifndef __DOLFIN_HEADER_ADAPTIVITY_H
#define __DOLFIN_HEADER_ADAPTIVITY_H

// DOLFIN adaptivity interface
#include <dolfin/adaptivity/AdaptiveRefinement.h>

#endif  /* __DOLFIN_HEADER_ADAPTIVITY_H */
