// Copyright (C) 2007-2008 Garth N. Wells.
// Licensed under the GNU LGPL Version 2.1.

#ifndef __DOLFIN_LU_SOLVER_H
#define __DOLFIN_LU_SOLVER_H

#include <dolfin/common/Timer.h>
#include <dolfin/config/dolfin_config.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/petsc/PETScLUSolver.h>
#include <dolfin/la/petsc/PETScMatrix.h>

namespace dolfin
{

//-----------------------------------------------------------------------------

class LUSolver
{

  /// LU solver for the built-in LA backends.

public:
  LUSolver()
  {
  }

  ~LUSolver()
  {
    delete petsc_solver;
  }

  auto solve( const GenericMatrix & A,
              GenericVector &       x,
              const GenericVector & b ) -> size_t
  {
    Timer timer( "LU solver" );

#ifdef HAVE_PETSC
    if ( A.has_type< PETScMatrix >() )
    {
      if ( !petsc_solver )
      {
        petsc_solver = new PETScLUSolver();
      }
      return petsc_solver->solve( A.down_cast< PETScMatrix >(),
                                  x.down_cast< PETScVector >(),
                                  b.down_cast< PETScVector >() );
    }
#endif
    error( "No default LU solver for given backend" );
    return 0;
  }

  auto factorize( const GenericMatrix & ) -> size_t
  {

    error( "No matrix factorization for given backend." );
    return 0;
  }

  auto factorized_solve( GenericVector &, const GenericVector & ) -> size_t
  {
    error( "No factorized LU solver for given backend." );
    return 0;
  }

private:
  // PETSc Solver
#ifdef HAVE_PETSC
  PETScLUSolver * petsc_solver { nullptr };
#else
  int * petsc_solver;
#endif
};

//-----------------------------------------------------------------------------

} // namespace dolfin

#endif
