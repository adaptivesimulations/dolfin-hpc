// Copyright (C) 2008 Garth N. Wells.
// Licensed under the GNU LGPL Version 2.1.

#ifndef __DOLFIN_PETSC_OBJECT_H
#define __DOLFIN_PETSC_OBJECT_H

#include <dolfin/main/SubSystemsManager.h>

namespace dolfin
{
  /// This class calls SubSystemsManger to initialise PETSc.
  ///
  /// All PETSc objects must be derived from this class.

  class PETScObject
  {
  public:

    PETScObject() { SubSystemsManager::PETSc::initialize(); }

  };

}

#endif

