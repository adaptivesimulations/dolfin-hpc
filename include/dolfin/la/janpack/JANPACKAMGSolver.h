// Copyright (C) 2012 Niclas Jansson.
// Licensed under the GNU LGPL Version 2.1.

#ifndef __DOLFIN_JANPACK_AMG_SOLVER_H
#define __DOLFIN_JANPACK_AMG_SOLVER_H

#include <dolfin/config/dolfin_config.h>

#if defined( HAVE_JANPACK ) && !defined( HAVE_JANPACK_MPI )

#include <dolfin/common/types.h>
#include <dolfin/la/MultigridScheme.h>
#include <dolfin/parameter/Parametrized.h>

#include <janpack/hybrid.h>
#include <janpack/amg_solver.h>

namespace dolfin
{
  /// Forward declarations
  class JANPACKMat;
  class JANPACKVec;

  class JANPACKAMGSolver : public Parametrized
  {
  public:

    /// Create AMG solver for a particular method and preconditioner
    JANPACKAMGSolver(MultigridScheme scheme, MultigridSmoother smoother,
		     MultigridCoarsening cscheme);

    ~JANPACKAMGSolver();

    /// Solve linear system Ax = b and return number of iterations
    size_t solve(const JANPACKMat& A, JANPACKVec& x, const JANPACKVec& b);

  private:

    /// Multigrid Scheme
    MultigridScheme scheme;

    /// Multigrid Smoother
    MultigridSmoother smoother;

    /// Multigrid Coarsening scheme
    MultigridCoarsening cscheme;

    /// Get JANPACK Multigrid scheme id
    int getScheme(MultigridScheme scheme) const;

    /// Get JANPACK Multigrid smoother id
    int getSmoother(MultigridSmoother smoother) const;

    // Get JANPACK Multigrid coarsening scheme id
    int getCoarsening(MultigridCoarsening cscheme) const;

    bool mls_init;

    // JANPACK multilevel solver cache
    char mls[JP_MLS_SIZE_T];
  };
}

#endif

#endif
