// Copyright (C) 2012 Niclas Jansson.
// Licensed under the GNU LGPL Version 2.1.

#ifndef __DOLFIN_MULTIGRID_SCHEME_H
#define __DOLFIN_MULTIGRID_SCHEME_H

namespace dolfin
{

  /// List of predefined multigrid schemes

  enum MultigridScheme
  {
    mgv,                // V-cycle scheme
    mgw,                // W-cycle scheme
    fmv,                // Full Multigrid V-cycle
    default_scheme      // Default multigrid scheme
  };

  enum MultigridCoarsening
  {
    rs,                 // Classic Rugen-Stueben
    cljp,               // Cleary-Luby-Jones-Plassman (CLJP) 
    pmis,               // Parallel Modified Independent Set 
    hmis,               // Hybrid Modified Independent Set 
    default_coarsening  // Default coarsening scheme
  };

  enum MultigridSmoother
  {
    mg_jacobi,          // Jacobi
    mg_gauss_seidel,    // Gauss-Seidel 
    mg_cf_gauss_seidel, // C/F colored Gauss-Seidel 
    mg_sor,             // SOR (successive over relaxation)
    mg_cf_sor,          // C/F colored SOR (successive over relaxation)
    default_smoother    // Default smoother
  };

}

#endif
