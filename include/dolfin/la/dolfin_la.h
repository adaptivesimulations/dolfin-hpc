
#ifndef __DOLFIN_HEADER_LA_H
#define __DOLFIN_HEADER_LA_H

// DOLFIN la interface

#include <dolfin/la/AMGSolver.h>
#include <dolfin/la/DefaultFactory.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericSparsityPattern.h>
#include <dolfin/la/GenericTensor.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/KrylovSolver.h>
#include <dolfin/la/LAPACKMatrix.h>
#include <dolfin/la/LAPACKVector.h>
#include <dolfin/la/LUSolver.h>
#include <dolfin/la/LinearAlgebraFactory.h>
#include <dolfin/la/LinearSolver.h>
#include <dolfin/la/Matrix.h>
#include <dolfin/la/MultigridScheme.h>
#include <dolfin/la/PreconditionerType.h>
#include <dolfin/la/Scalar.h>
#include <dolfin/la/SolverType.h>
#include <dolfin/la/SparsityPattern.h>
#include <dolfin/la/Vector.h>
#include <dolfin/la/VectorNormType.h>
#include <dolfin/la/janpack/JANPACKAMGSolver.h>
#include <dolfin/la/janpack/JANPACKFactory.h>
#include <dolfin/la/janpack/JANPACKKrylovSolver.h>
#include <dolfin/la/janpack/JANPACKMat.h>
#include <dolfin/la/janpack/JANPACKVec.h>
#include <dolfin/la/petsc/PETScFactory.h>
#include <dolfin/la/petsc/PETScKrylovSolver.h>
#include <dolfin/la/petsc/PETScLUSolver.h>
#include <dolfin/la/petsc/PETScMatrix.h>
#include <dolfin/la/petsc/PETScObject.h>
#include <dolfin/la/petsc/PETScVector.h>
#include <dolfin/la/trilinos/TrilinosObject.h>
#include <dolfin/la/trilinos/TrilinosVector.h>

#endif /* __DOLFIN_HEADER_LA_H */
