// Copyright (C) 2005-2006 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.

#ifndef __DOLFIN_BOX_H
#define __DOLFIN_BOX_H

#include <dolfin/mesh/Mesh.h>

namespace dolfin
{

/// Tetrahedral mesh of the 3D  rectangular prism (a,b) x (c,d) x (e,f).
/// Given the number of cells (nx, ny, nz) in each direction,
/// the total number of tetrahedra will be 6*nx*ny*nz and the
/// total number of vertices will be (nx + 1)*(ny + 1)*(nz + 1).

class Box : public Mesh
{
public:
  Box( real   a,
       real   b,
       real   c,
       real   d,
       real   e,
       real   f,
       size_t nx,
       size_t ny,
       size_t nz );
};

}

#endif
