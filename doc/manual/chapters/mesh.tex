\chapter{The mesh}


\section{Basic concepts}

\subsection{Mesh}

A \emph{mesh} consists of \emph{mesh topology}, \emph{mesh geometry}
and \emph{mesh overlap} (when running in parallel).  These concepts
are implemented by the classes \texttt{Mesh}, \texttt{MeshTopology},
\texttt{MeshGeometry} and \texttt{MeshDistributedData}.

\subsection{Mesh entities}

% FIXME: Add some nice figures here

A \emph{mesh entity} is a pair $(d, i)$, where $d$ is the topological
dimension of the mesh entity and $i$ is a unique index of the mesh
entity. Mesh entities are numbered within each topological dimension
from $0$ to $n_d-1$, where $n_d$ is the number of mesh entities of
topological dimension $d$.

For convenience, mesh entities of topological dimension $0$ are
referred to as \emph{vertices}, entities of dimension $1$
\emph{edges}, entities of dimension $2$ \emph{faces}, entities of
\emph{codimension} $1$ \emph{facets} and entities of codimension $0$
\emph{cells}. These concepts are summarized in
Table~\ref{tab:entities,here}.

\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{|l|c|c|}
      \hline
      Entity & Dimension & Codimension \\
      \hline
      Vertex & $0$       & -- \\
      Edge   & $1$       & -- \\
      Face   & $2$       & -- \\
      & & \\
      Facet  & --      &  $1$ \\
      Cell   & --      &  $0$ \\
      \hline
    \end{tabular}
    \caption{Named mesh entities.}
    \label{tab:entities,here}
  \end{center}
\end{table}

These concepts are implemented by the classes
\texttt{MeshEntity},
\texttt{Vertex},
\texttt{Edge},
\texttt{Face},
\texttt{Facet},
\texttt{Cell}.

\section{Mesh iterators}

Algorithms operating on a mesh
can often be expressed in terms of
\emph{iterators}. The mesh library provides the general iterator
\texttt{MeshEntityIterator} for iteration over mesh entities, as well
as the specialized mesh iterators
\texttt{VertexIterator},
\texttt{EdgeIterator},
\texttt{FaceIterator},
\texttt{FacetIterator} and
\texttt{Cell\-Iterator}.

The following code illustrates how to iterate over all incident
(connected) vertices of all vertices of all cells of a given mesh:
\begin{code}
for (CellIterator c(mesh); !c.end(); ++c)
  for (VertexIterator v0(*c); !v0.end(); ++v0)
    for (VertexIterator v1(*v0); !v1.end(); ++v1)
      cout << *v1 << endl;
\end{code}
This may alternatively be implemented using the general iterator
\texttt{MeshEntity\-Iterator} as follows:
\begin{code}
unsigned int dim = mesh.topology().dim();
for (MeshEntityIterator c(mesh, dim); !c.end(); ++c)
  for (MeshEntityIterator v0(*c, 0); !v0.end(); ++v0)
    for (MeshEntityIterator v1(*v0, 0); !v1.end(); ++v1)
      cout << *v1 << endl;
\end{code}

\section{Mesh functions}

A \texttt{MeshFunction} represents a discrete function that takes a
value on each mesh entity of a given topological dimension.
A \texttt{MeshFunction} may for example be used to store a global
numbering scheme for the entities of a (parallel) mesh, marking
sub domains or boolean markers for mesh refinement.

\section{Mesh refinement}

A mesh may be refined uniformly as follows:
\begin{code}
mesh.refine();
\end{code}
A mesh may also be refined locally by supplying a
\texttt{MeshFunction} with boolean markers for the cells that should
be refined as follows:
\begin{code}
mesh.refine(cell_marker);
\end{code}

\section{Working with meshes}

\subsection{Reading a mesh from file}

A mesh may be loaded from a file, either by specifying the file name
to the constructor of the class \texttt{Mesh}:
\begin{code}
Mesh mesh("mesh.xml");
\end{code}
or by creating a \texttt{File} object and streaming to a
\texttt{Mesh}:
\begin{code}
File file("mesh.xml");
Mesh mesh;
file >> mesh;
\end{code}
A mesh may be stored to file as follows:
\begin{code}
File file("mesh.xml");
Mesh mesh;
file << mesh;
\end{code}

The \dolfin{} mesh XML format has changed in \dolfin{} version
0.6.3. Meshes in the old XML format may be converted to the new XML
format using the script \texttt{dolfin-convert} included in the
distribution of \dolfin{}. For instructions, type
\texttt{dolfin-convert --help}.

\subsection{Extracting a boundary mesh}

For any given mesh, a mesh of the boundary of the mesh (if any) may be
created as follows:
\begin{code}
BoundaryMesh boundary(mesh);
\end{code}
A \texttt{BoundaryMesh} is itself a \texttt{Mesh} of the same
geometrical dimension and has the topological dimension of the mesh
minus one.

The computation of a boundary mesh may also provide mappings from the
vertices of the boundary mesh to the corresponding vertices in the
original mesh, and from the cells of the boundary mesh to the
corresponding facets of the original mesh:
\begin{code}
BoundaryMesh boundary(mesh, BoundaryMesh::exterior);
\end{code}

\subsection{Built-in meshes}

\dolfin{} provides functionality for creating simple meshes, such as
the mesh of the unit square and the unit cube. The following code
demonstrates how to create a $16\times 16$ triangular mesh of the unit square
(consisting of $2\times 16\times 16 = 512$ triangles) and a
$16\times 16\times 16$ tetrahedral mesh of the unit cube (consisting
of $6\times 16\times 16\times 16 = 24576$ tetrahedra):
\begin{code}
UnitInterval mesh1D(16);
UnitSquare mesh2D(16, 16);
UnitCube mesh3D(16, 16, 16);
\end{code}

\devnote{We could easily add other built-in meshes, like the unit disc,
  the unit sphere, rectangles, blocks etc.
  Any contributions are welcome.}

\subsection{Creating meshes}

Simplicial meshes (meshes consisting of intervals, triangles or
tetrahedra) may be constructed explicitly by specifying the
cells and vertices of the mesh. A specialized interface for creating
simplicial meshes is provided by the class \texttt{MeshEditor}.
The following code demonstrates how to create a very simple mesh
consisting of two triangles covering the unit square:
\begin{code}
Mesh mesh;
MeshEditor editor(mesh, CellType::triangle, 2);
editor.init_vertices(4);
editor.init_cells(2);
real coords[4][2] = {{0.0,0.0},
                     {1.0,0.0},
                     {1.0,1.0},
                     {0.0,1.0}};
editor.add_vertex(0, coords[0]);
editor.add_vertex(1, coords[1]);
editor.add_vertex(2, coords[2]);
editor.add_vertex(3, coords[3]);
uint cells[2][3] = {{0, 1, 2},
                    {0, 2, 3}};

editor.add_cell(0, cells[0]);
editor.add_cell(1, cells[1]);
editor.close();
\end{code}
Note that the \dolfin{} mesh library is not specialized to simplicial
meshes, but supports general collections of mesh entities. However,
tools like mesh refinement and mesh editors are currently only
available for simplicial meshes.
