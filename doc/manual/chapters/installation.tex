\chapter{Installation}
\label{app:installation}
\index{installation}

The source code of \dolfin{} is portable and should compile on any
Unix system, although it is developed mainly under GNU/Linux (in 
particular Debian GNU/Linux). \dolfin{} can be compiled under Windows
through Cygwin~\cite{www:Cygwin}. Questions, bug reports and patches
concerning the installation should be directed to the \dolfin{} mailing 
list at the address
\begin{code}
dolfin-dev@fenics.org
\end{code}

\dolfin{} must currently be compiled directly from source, but an effort
is underway to provide precompiled Debian packages of \dolfin{} and
other \fenics{} components.

%------------------------------------------------------------------------------
\section{Installing from source}

\subsection{Dependencies and requirements}
\index{dependencies}

\dolfin{} depends on a number of libraries that need to be installed on your
system. These libraries include Boost, Libxml2 and optionally PETSc and UMFPACK. 
If you wish to use the Python interface \pydolfin{}, the libraries SWIG 
and NumPy are required. In addition to these 
libraries, you need to install \fiat{} and \ffc{} if you want to define your 
own variational forms.

\subsubsection{Installing Boost}
\index{Boost}

Boost is a collection of C++ source libraries. Boost can be obtained from
\begin{code}
http://www.boost.org/
\end{code}
Packages are available for most Linux distributions. For Ubuntu/Debian users, the 
package to install is \texttt{boost-dev}.

\subsubsection{Installing Libxml2}
\index{Libxml2}

Libxml2 is a library used by \dolfin{} to parse XML data files. Libxml2 can be
obtained from
\begin{code}
http://xmlsoft.org/
\end{code}
Packages are available for most Linux distributions. For Ubuntu/Debian users, the 
package to install is \texttt{libxml2-dev}.


\subsubsection{Installing NumPy}
\index{NumPy}

NumPy is required for generating the Python interface \pydolfin{}. 
It can be obtained from
\begin{code}
http://numpy.scipy.org/
\end{code}
Packages are available for most Linux distributions. For Ubuntu/Debian users,
the packages \texttt{python-numpy} \texttt{python-numpy-ext} should be installed.


\subsubsection{Installing SWIG}
\index{SWIG}

SWIG is also required for generating the Python interface \pydolfin{}. 
It can be obtained from
\begin{code}
http://www.swig.org/
\end{code}
Packages are available for most Linux distributions. For Ubuntu/Debian users,
the package \texttt{swig} should be installed.


\subsubsection{Installing UMFPACK}
\index{UMFPACK}

UMFAPCK is part of the SuiteSparse collection and is a library for the direct 
solution of linear systems. It is highly recommended if PESTSc is not installed. 
If PETSc is not installed, UMFPACK is the default direct linear solver.
The SuiteSparse collection can be downloaded from
\begin{code}
http://www.cise.ufl.edu/research/sparse/SuiteSparse/
\end{code}
UMFPACK is available as a package for many Linux distributions. For 
Ubuntu/Debian users, the packages \texttt{libumfpack4} and 
\texttt{libumfpack4-dev} should be installed. For other distributions, the package
may be called \texttt{libufsparse}.


\subsubsection{Installing PETSc}
\index{PETSc}

Optionally, \dolfin{} may be compiled with support for PETSc. To
compile \dolfin{} with PETSc, add the flag \texttt{--enable-petsc}
during the initial configuration of \dolfin{}.

PETSc is a library for the solution of linear and nonlinear systems, functioning
as the backend for the \dolfin{} linear algebra classes. \dolfin{} depends on 
PETSc version 2.3.1, which can be obtained from
\begin{code}
http://www-unix.mcs.anl.gov/petsc/petsc-2/
\end{code}

Follow the installation instructions on the PETSc web page. Normally,
you should only have to perform the following simple steps in the PETSc
source directory:
\begin{code}
# export PETSC_DIR=`pwd`
# ./config/configure.py --with-clanguage=cxx --with-shared=1
# make all
\end{code}

Add \texttt{--download-hypre=yes} to configure.py if you want to
install Hypre which provides a collection of preconditioners,
including algebraic multigrid (AMG), and 
\texttt{--download-umfpack=yes} to configure.py if you want to
install UMFPACK which provided as fast direct linear solver.
Both packages are highly recommended.

DOLFIN assumes that \texttt{PETSC\_DIR} is \texttt{/usr/local/lib/petsc/} but
this can be controlled using the flag \texttt{--with-petsc-dir=<path>} when 
configuring DOLFIN (see below).

\subsubsection{Installing FFC}
\index{FFC}

\dolfin{} uses the FEniCS Form Compiler \ffc{} to process variational
forms. \ffc{} can be obtained from
\begin{code}
http://www.fenics.org/
\end{code}

Follow the installation instructions given in the \ffc{}
manual. \ffc{} follows the standard for Python packages, which means
that normally you should only have to perform the following simple step
in the \ffc{} source directory:
\begin{code}
# python setup.py install
\end{code}

Note that \ffc{} depends on \fiat{} \index{FIAT}, which in turn depends on
the Python packages NumPy~\cite{www:numpy} (Debian package \texttt{python-numpy}) and
LinearAlgebra (Debian package \texttt{python-numpy-ext}). Refer to
the \ffc{} manual for further details.

% Input section shared with FFC manual
\input{chapters/installation-downloading.tex}

\subsection{Compiling the source code}
\index{compiling}

\dolfin{} is built using the standard GNU Autotools (Automake,
Autoconf) and libtool, which means that the installation procedure is simple:
\begin{code}
# ./configure
# make
\end{code}
followed by an optional
\begin{code}
# make install
\end{code}
to install \dolfin{} on your system.

The configure script will check for a number of libraries and try
to figure out how compile \dolfin{} against these libraries. The
configure script accepts a collection of optional arguments that can be
used to control the compilation process. A few of these are listed
below. Use the command
\begin{code}
# ./configure --help
\end{code}
for a complete list of arguments.

\begin{itemize}
\item
  Use the option \texttt{--prefix=<path>} to specify an alternative
  directory for installation of \dolfin{}. The default directory is
  \texttt{/usr/local/}, which means that header files will be
  installed under \texttt{/usr/local/include/} and libraries will be
  installed under \texttt{/usr/local/lib/}. This option can be useful
  if you don't have root access but want to install \dolfin{} locally
  on a user account as follows:
  \begin{code}
# mkdir ~/local
# ./configure --prefix=~/local
# make
# make install
  \end{code}
\item
  Use the option \texttt{--enable-debug} to compile \dolfin{} with
  debugging symbols and assertions.
\item
  Use the option \texttt{--enable-optimization} to compile an
  optimized version of \dolfin{} without debugging symbols
  and assertions.
\item
  Use the option \texttt{--disable-curses} to compile \dolfin{}
  without the curses interface (a text-mode graphical user interface).
\item
  Use the option \texttt{--enable-petsc} to compile \dolfin{} with
  support for PETSc.
\item
  Use the option \texttt{--disable-pydolfin} to compile without
  support for PyDOLFIN.
\item
  Use the option \texttt{--disable-mpi} to compile \dolfin{} without
  support for MPI (Message Passing Interface), assuming PETSc has been
  compiled without support for MPI.
\item
  Use the option \texttt{--with-petsc-dir=<path>} to specify the
  location of the PETSc directory. By default, \dolfin{} assumes that
  PETSc has been installed in \texttt{/usr/local/lib/petsc/}.
\end{itemize}

\subsection{Compiling the demo programs}
\index{demo programs}

After compiling the \dolfin{} library according to the instructions
above, you may want to try one of the demo programs in the
subdirectory \texttt{src/demo/} of the \dolfin{} source tree.
Just enter the directory containing the demo program you want to
compile and type \texttt{make}. You may also compile all demo programs
at once using the command
\begin{code}
# make demo
\end{code}

\subsection{Compiling a program against \dolfin{}}
\index{compiling}

Whether you are writing your own Makefiles or using an automated build
system such as GNU Autotools or BuildSystem, it is straightforward to
compile a program against \dolfin{}. The necessary include and library
paths can be obtained through the script \texttt{dolfin-config} which
is automatically generated during the compilation of \dolfin{} and
installed in the \texttt{bin} subdirectory of the \texttt{<path>}
specified with \texttt{--prefix}. Assuming this directory is in your
executable path (environment variable \texttt{PATH}), the include
path for building \dolfin{} can be obtained from the command
\begin{code}
dolfin-config --cflags
\end{code}
and the path to \dolfin{} libraries can be obtained from the command
\begin{code}
dolfin-config --libs
\end{code}
If \texttt{dolfin-config} is not in your executable path, you need to
provide the full path to \texttt{dolfin-config}.

Examples of how to write a proper \texttt{Makefile} are provided with
each of the example programs in the subdirectory \texttt{src/demo/} in
the \dolfin{} source tree.

%------------------------------------------------------------------------------
\section{Debian package}
\index{Debian package}

In preparation.

%------------------------------------------------------------------------------
\section{Installing from source under Windows}
\label{app:cygwin}
\index{Cygwin}

\dolfin{} can be used under Windows using Cygwin, which provides a Linux-like
environment. The installation process is the same as under GNU/Linux. 
To use \dolfin{} under Cygwin, the Cygwin development tools must be installed. 
Instructions for installing PETSc under Cygwin can be found on the  PETSc web 
page. Installation of \ffc{} and \fiat{} is the same as under GNU/Linux. The 
Python package NumPy is not available as a Cygwin package and must be 
installed manually. To compile \dolfin{}, the Cygwin package  
\texttt{libxml2-devel} must be installed. For \pydolfin{} the 
package \texttt{swig} must be installed. \texttt{NumPy} is not available as 
a package for Cygwin, therefore it must be installed manually if you wish to 
use \pydolfin{}.
The compilation procedure is then the
same as under GNU/Linux. If MPI has not been installed:
\begin{code}
# ./configure --disable-mpi
# make
\end{code}
followed by an optional
\begin{code}
# make install
\end{code}
will compile \dolfin{} on your system.
